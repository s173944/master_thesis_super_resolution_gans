import torch
import torchvision
import torchvision.transforms as transforms
import torchio as tio
import torchio.transforms as tiotransforms
import os
import pickle
import random

# First lets check if a GPU is available
DEVICE = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Define global variables for other scripts
DO_TRAIN = 0
LEARNING_RATE = 10e-4  # 1e-4
IN_C = 1  # Number of image color channels, 1 for grayscale input
GAUSS_K_SIZE = 15
GAUSS_SIGMA = 1.0  # 1.0 for non-bone data
LOW_RES = 64  # Maximum size is 64 for 8GB VRAM
UP_FACTOR = 4
HIGH_RES = LOW_RES*UP_FACTOR
BATCH_SIZE = 3  # In mDCSRN-GAN paper, batch size is set to 6
EPOCHS = 20

PATCH_SIZE = 20
PATCH_SIZE_HR = PATCH_SIZE*UP_FACTOR
K_FACTOR = 12  # 6
K_SIZE = 3

LOAD_PREVIOUS_MODELS = 1
LOAD_LATEST_CHECKPOINT = 0
if LOAD_PREVIOUS_MODELS or LOAD_LATEST_CHECKPOINT:
    ID_LOAD = 100195  # HCP 1200
    ID_LOAD = 100305  # Femur
    EXPERIMENT_NAME_LOAD = "RUN_ID%d" % ID_LOAD  # Choose the experiment you would like to load

    ID = 100203  # 100150
    if DO_TRAIN == 1:
        EXPERIMENT_NAME = "PROCEEDING_ID%d_AS_RUN_ID%d" % (ID_LOAD, ID)
    else:
        EXPERIMENT_NAME = "RUN_ID%d" % ID_LOAD
    #print(EXPERIMENT_NAME)
else:
    # Experiment name
    # generate 6 digit ID
    #ID = random.randint(100000, 999999)
    ID = 100305  #  100150
    EXPERIMENT_NAME = "RUN_ID%d" % ID

UPSAMPLE_METHOD = 'DECONV_NN_RESIZE'
#UPSAMPLE_METHOD = None
DO_FULL_RECONSTRUCTION = 0
RECONSTRUCTION_IDX = 0
VALIDATION_FREQUENCY = 1
CHECKPOINT_FREQUENCY = 25

# Loss weigths:
LOSS_WEIGHTS = {
    "MSE": 0,  # 1e-2 for WGAN-GP
    "L1": 1.0,  # 1e-2 for WGAN-GP. L1 loss is used in mDCSRN-GAN paper as opposed to L2/MSE loss
    "BCE_Logistic": 1.0,
    "BCE": 1.0,
    "VGG": 0.006,
    "VGG3D": 0,  # 0.006,
    "GRAD": 0,  # 0.1
    "LAPLACE": 0,
    "TV3D": 0,  # 0.1
    "TEXTURE3D": 0,  # 0.5
    "ADV": 10**-3,  # 10**-3,  # is 0.1 in mDCSRN-GAN paper, but uses WGAN-GP instead of vanilla GAN
    "STRUCTURE_TENSOR": 0  # 10**-7
}

DISABLE_LOSSES_FROM_EPOCH = EPOCHS+1

# WGAN-GP hyperparameters
LAMBDA_GP = 10
CRITIC_ITERATIONS = 7
ALPHA_WGAN = 0.0001
BETA1_WGAN = 0
BETA2_WGAN = 0.9

# Dataset parameters
use_dataset = "2022_QIM_52_Bone"  # Use this select the dataset for DCSRN
#use_dataset = "HCP_1200"
#use_dataset = "IXI"

if use_dataset == "HCP_1200":
    DATASET_PATH = "3D_datasets/datasets/HCP_1200"
    TRAIN_PATH = "train"
    TEST_PATH = "test"

    OUT_DICT_FILENAME = "HCP_1200_out_dict_LR%d_EP%d_BS%d.pkl" % (PATCH_SIZE, EPOCHS, BATCH_SIZE)
    GENERATOR_NAME = "HCP_1200_generator_LR%d_EP%d_BS%d.pt" % (PATCH_SIZE, EPOCHS, BATCH_SIZE)
    DISCRIMINATOR_NAME = "HCP_1200_discriminator_LR%d_EP%d_BS%d.pt" % (PATCH_SIZE, EPOCHS, BATCH_SIZE)
    OUT_DIR_NAME = "HCP_1200"
    IMAGE_FORMAT = ".nii"
    DIM = [260, 311, 260]

elif use_dataset == "HCP_retest":
    DATASET_PATH = "3D_datasets/datasets/HCP_retest"
    TRAIN_PATH = "train"
    TEST_PATH = "test"

    OUT_DICT_FILENAME = "HCP_retest_out_dict_LR%d_EP%d_BS%d.pkl" % (PATCH_SIZE, EPOCHS, BATCH_SIZE)
    GENERATOR_NAME = "HCP_retest_generator_LR%d_EP%d_BS%d.pt" % (PATCH_SIZE, EPOCHS, BATCH_SIZE)
    DISCRIMINATOR_NAME = "HCP_retest_discriminator_LR%d_EP%d_BS%d.pt" % (PATCH_SIZE, EPOCHS, BATCH_SIZE)
    OUT_DIR_NAME = "HCP_retest"
    IMAGE_FORMAT = ".nii"
    DIM = [320, 256, 256]

elif use_dataset == "IXI":
    DATASET_PATH = "3D_datasets/datasets/IXI"
    TRAIN_PATH = "train"
    TEST_PATH = "test"

    OUT_DICT_FILENAME = "IXI_out_dict_LR%d_EP%d_BS%d.pkl" % (PATCH_SIZE, EPOCHS, BATCH_SIZE)
    GENERATOR_NAME = "IXI_generator_LR20_EP100.pt"
    DISCRIMINATOR_NAME = "IXI_discriminator_LR20_EP100.pt"
    OUT_DIR_NAME = "IXI"
    IMAGE_FORMAT = ".nii"
    DIM = [256, 256, 150]
elif use_dataset == "2022_QIM_52_Bone":

    GAUSS_SIGMA = 0.0  # Remember to change this if upscaling factor is 1
    DATASET_PATH = "3D_datasets/datasets/2022_QIM_52_Bone"
    TRAIN_PATH = "train"
    TEST_PATH = "test"
    IMAGE_FORMAT = ".nii"

    GENERATOR_NAME = "Bone_gen_LR%d_EP%d_BS%d.pt" % (PATCH_SIZE, EPOCHS, BATCH_SIZE)
    DISCRIMINATOR_NAME = "Bone_disc_LR%d_EP%d_BS%d.pt" % (PATCH_SIZE, EPOCHS, BATCH_SIZE)
    OUT_DIR_NAME = "2022_QIM_52_Bone"
    OUT_DICT_FILENAME = "Bone_out_dict_LR%d_EP%d_BS%d.pkl" % (PATCH_SIZE, EPOCHS, BATCH_SIZE)

    SAMPLING_METHOD = "label"

    NORMALIZATION_METHOD = "block_wise"

    PRELOAD_BONE_DATA = False
    PRELOAD_BONE_DATA_TEST = True  # ONLY FOR HOME PC
    COMPUTE_MEAN_STD = False

    if PRELOAD_BONE_DATA:
        data_path_train = os.path.join(DATASET_PATH, TRAIN_PATH)
        femur_01_b_1 = tio.Subject(
            hi_res=tio.ScalarImage(os.path.join(data_path_train, "resliced_femur_01_b_1/cropped_femur_01_b 1_int16.nii")),
            lo_res=tio.ScalarImage(os.path.join(data_path_train, "resliced_femur_01_b_1/aligned_resliced_clinical_femur_01_b_1.nii")),
            seg=tio.LabelMap(os.path.join(data_path_train, "resliced_femur_01_b_1/aligned_resliced_femur_01_b_1_segmentation_1.nii")),
            name="femur_01_b_1")

        femur_15_1 = tio.Subject(
            hi_res=tio.ScalarImage(os.path.join(data_path_train, "resliced_femur_15_1/cropped_femur_15 1_int16.nii")),
            lo_res=tio.ScalarImage(os.path.join(data_path_train, "resliced_femur_15_1/aligned_resliced_clinical_femur_15_1.nii")),
            seg=tio.LabelMap(os.path.join(data_path_train, "resliced_femur_15_1/aligned_resliced_femur_15_1_segmentation_2.nii")),
            name="femur_15_1")

        femur_21_1 = tio.Subject(
            hi_res=tio.ScalarImage(os.path.join(data_path_train, "resliced_femur_21_1/cropped_femur_21 1_int16.nii")),
            lo_res=tio.ScalarImage(os.path.join(data_path_train, "resliced_femur_21_1/aligned_resliced_clinical_femur_21_1.nii")),
            seg=tio.LabelMap(os.path.join(data_path_train, "resliced_femur_21_1/aligned_resliced_femur_21_1_segmentation_1.nii")),
            name="femur_21_1")

        femur_74_c_1 = tio.Subject(
            hi_res=tio.ScalarImage(os.path.join(data_path_train, "resliced_femur_74_c_1/cropped_femur_74_c 1_int16.nii")),
            lo_res=tio.ScalarImage(os.path.join(data_path_train, "resliced_femur_74_c_1/aligned_resliced_clinical_femur_74_c_1.nii")),
            seg=tio.LabelMap(os.path.join(data_path_train, "resliced_femur_74_c_1/aligned_resliced_femur_74_c_1_segmentation_1.nii")),
            name="femur_74_c_1")

        SUBJECTS_LIST = [femur_01_b_1, femur_15_1, femur_21_1, femur_74_c_1]

        ## PRELOAD
        # for i, subject in enumerate(SUBJECTS_LIST):
        #    print("LOADING SUBJECT %d" % (i+1))
        #    subject.load()
        if COMPUTE_MEAN_STD:
            for i, subject in enumerate(SUBJECTS_LIST):
                # PRE-COMPUTE MEAN AND STANDARD DEVIATION OF HIGH/LOW RES
                print("COMPUTING SUBJECT %d MEAN AND STANDARD DEVIATIONS" % i)
                mean_hi_res = subject['hi_res'].data.float().mean()
                mean_lo_res = subject['lo_res'].data.float().mean()
                std_hi_res = subject['hi_res'].data.float().std()
                std_lo_res = subject['lo_res'].data.float().std()

                subject['mean_hi_res'] = mean_hi_res
                subject['mean_lo_res'] = mean_lo_res
                subject['std_hi_res'] = std_hi_res
                subject['std_lo_res'] = std_lo_res

                mean_std_dict = {
                    "mean_hi_res": mean_hi_res,
                    "mean_lo_res": mean_lo_res,
                    "std_hi_res": std_hi_res,
                    "std_lo_res": std_lo_res,
                }

                print("SAVING SUBJECT %d MEAN AND STANDARD DEVIATIONS" % i)
                file_string = subject["name"] + '_mean_std.pkl'
                with open(os.path.join("normalization_values", file_string), 'wb') as handle:
                    pickle.dump(mean_std_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)

        else:
            for i, subject in enumerate(SUBJECTS_LIST):
                file_string = subject["name"] + '_mean_std.pkl'
                with open(os.path.join("normalization_values", file_string), 'rb') as handle:
                    mean_std_dict = pickle.load(handle)

                subject['mean_hi_res'] = mean_std_dict["mean_hi_res"]
                subject['mean_lo_res'] = mean_std_dict["mean_lo_res"]
                subject['std_hi_res'] = mean_std_dict["std_hi_res"]
                subject['std_lo_res'] = mean_std_dict["std_lo_res"]
        
        #for subject in SUBJECTS_LIST:
        #    print("hi_res spacing" + subject["hi_res"].spacing)
        #    print("lo_res spacing" + subject["lo_res"].spacing)
        #    print("seg spacing" + subject["seg"].spacing)

        SUBJECTS_DATASET = tio.SubjectsDataset([femur_01_b_1, femur_15_1, femur_21_1])  # Keep femur_74_c_1 as test data

        SUBJECTS_LIST_TEST = [femur_74_c_1]
        SUBJECTS_DATASET_TEST = tio.SubjectsDataset(SUBJECTS_LIST_TEST)

        if SAMPLING_METHOD == "weighted":
            SAMPLER = tio.data.WeightedSampler(PATCH_SIZE_HR, probability_map="seg")
        elif SAMPLING_METHOD == "label":
            SAMPLER = tio.data.LabelSampler(PATCH_SIZE_HR, label_name="seg")
        else:
            SAMPLER = tio.data.UniformSampler(PATCH_SIZE_HR)

        DOWNSCALE_FUNC = tiotransforms.Resize(target_shape=(PATCH_SIZE_HR // UP_FACTOR), image_interpolation='LINEAR')

    elif PRELOAD_BONE_DATA_TEST:

        print("DCSRN_CONFIG")
        data_path_train = os.path.join(DATASET_PATH, TRAIN_PATH)
        femur_01_b_1 = tio.Subject(
            hi_res=tio.ScalarImage(os.path.join(data_path_train, "raw/clinical/clinical_femur01.nii")),
            lo_res=tio.ScalarImage(os.path.join(data_path_train, "raw/clinical/clinical_femur01.nii")),
            seg=tio.LabelMap(os.path.join(data_path_train, "raw/segmentations/clinical_femur_01_b_1_segmentation_1.nii.gz")),
            name="femur_01_b_1")

        femur_15_1 = tio.Subject(
            hi_res=tio.ScalarImage(os.path.join(data_path_train, "raw/clinical/clinical_femur15.nii")),
            lo_res=tio.ScalarImage(os.path.join(data_path_train, "raw/clinical/clinical_femur15.nii")),
            seg=tio.LabelMap(os.path.join(data_path_train, "raw/segmentations/clinical_femur_15_1_segmentation_1.nii.gz")),
            name="femur_15_1")

        femur_21_1 = tio.Subject(
            hi_res=tio.ScalarImage(os.path.join(data_path_train, "raw/clinical/clinical_femur21.nii")),
            lo_res=tio.ScalarImage(os.path.join(data_path_train, "raw/clinical/clinical_femur21.nii")),
            seg=tio.LabelMap(os.path.join(data_path_train, "raw/segmentations/clinical_femur_21_1_segmentation_1.nii.gz")),
            name="femur_21_1")

        femur_74_c_1 = tio.Subject(
            hi_res=tio.ScalarImage(os.path.join(data_path_train, "raw/clinical/clinical_femur74.nii")),
            lo_res=tio.ScalarImage(os.path.join(data_path_train, "raw/clinical/clinical_femur74.nii")),
            seg=tio.LabelMap(os.path.join(data_path_train, "raw/segmentations/clinical_femur_74_c_1_segmentation_1.nii.gz")),
            name="femur_74_c_1")

        SUBJECTS_LIST = [femur_01_b_1, femur_15_1, femur_21_1, femur_74_c_1]

        ## PRELOAD
        #for i, subject in enumerate(SUBJECTS_LIST):
        #    print("LOADING SUBJECT %d" % (i+1))
        #    subject.load()
        if COMPUTE_MEAN_STD:
            for i, subject in enumerate(SUBJECTS_LIST):
                # PRE-COMPUTE MEAN AND STANDARD DEVIATION OF HIGH/LOW RES
                print("COMPUTING SUBJECT %d MEAN AND STANDARD DEVIATIONS" % i)
                mean_hi_res = subject['hi_res'].data.float().mean()
                mean_lo_res = subject['lo_res'].data.float().mean()
                std_hi_res = subject['hi_res'].data.float().std()
                std_lo_res = subject['lo_res'].data.float().std()

                subject['mean_hi_res'] = mean_hi_res
                subject['mean_lo_res'] = mean_lo_res
                subject['std_hi_res'] = std_hi_res
                subject['std_lo_res'] = std_lo_res

                mean_std_dict = {
                    "mean_hi_res": mean_hi_res,
                    "mean_lo_res": mean_lo_res,
                    "std_hi_res": std_hi_res,
                    "std_lo_res": std_lo_res,
                }

                print("SAVING %d MEAN AND STANDARD DEVIATIONS" % i)
                file_string = subject["name"] + '_mean_std.pkl'
                with open(os.path.join("normalization_values", file_string), 'wb') as handle:
                    pickle.dump(mean_std_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)

            COMPUTE_MEAN_STD = False

        else:
            for i, subject in enumerate(SUBJECTS_LIST):
                file_string = subject["name"] + '_mean_std.pkl'
                with open(os.path.join("normalization_values", file_string), 'rb') as handle:
                    mean_std_dict = pickle.load(handle)

                subject['mean_hi_res'] = mean_std_dict["mean_hi_res"]
                subject['mean_lo_res'] = mean_std_dict["mean_lo_res"]
                subject['std_hi_res'] = mean_std_dict["std_hi_res"]
                subject['std_lo_res'] = mean_std_dict["std_lo_res"]

        #for subject in SUBJECTS_LIST:
        #    print(subject["hi_res"].spacing)
        #    print(subject["lo_res"].spacing)
        #    print(subject["seg"].spacing)

        SUBJECTS_DATASET = tio.SubjectsDataset([femur_01_b_1, femur_15_1, femur_21_1])

        SUBJECTS_LIST_TEST = [femur_74_c_1]
        SUBJECTS_DATASET_TEST = tio.SubjectsDataset(SUBJECTS_LIST_TEST)

        if SAMPLING_METHOD == "weighted":
            SAMPLER = tio.data.WeightedSampler(PATCH_SIZE_HR, probability_map="seg")
        elif SAMPLING_METHOD == "label":
            SAMPLER = tio.data.LabelSampler(PATCH_SIZE_HR, label_name="seg")
        else:
            SAMPLER = tio.data.UniformSampler(PATCH_SIZE_HR)

        DOWNSCALE_FUNC = tiotransforms.Resize(target_shape=(PATCH_SIZE_HR // UP_FACTOR), image_interpolation='LINEAR')
    else:
        NORMALIZATION_METHOD = "block_wise"

else:
    print("Error, please select a valid dataset")

# Non random transformations. The random transformations should be added in the dataset
TRANSFORM_NORM_HR_RGB = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

TRANSFORM_NORM_LR_RGB = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0, 0, 0), (1, 1, 1))])

TRANSFORM_NORM_HR_GRAY = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize(0.5, 0.5)])  # Should be used instead if Normalization is performed.

TRANSFORM_NORM_LR_GRAY = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize(0, 1)])  # Should be used instead if Normalization is performed.

TRANSFORM_TENSOR = transforms.Compose([
    transforms.ToTensor()])

TRANSFORM_Z_NORM = transforms.Compose([tio.ZNormalization()])

#TRANSFORM = TRANSFORM_NORM_HR_RGB

# LR images are normalized to [0; 1] and HR images are normalized to [-1, 1]
TRANSFORM_LR = TRANSFORM_Z_NORM
TRANSFORM_HR = TRANSFORM_Z_NORM

# Add random transformations and non-random gaussian blur if desired
#T_LIST = ["random_crop", "hflip", "vflip", "gaussian_blur"]

T_LIST = ["random_crop", "gaussian_blur"]
