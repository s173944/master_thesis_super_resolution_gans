import copy

import PIL.Image as Image
import os
import glob
import torch
from torch.utils.data import DataLoader
import random
import torchvision.transforms.functional as TF
import torch.nn.functional as F
import torchvision.transforms as transforms
import numpy as np
import matplotlib.pyplot as plt
import DCSRN_config
import nibabel as nib
import torchio as tio
import torchio.transforms as tiotransforms

from utils import load_patch_subjects_V3, test_3d_gaussian_blur

def nvg_func(use_dataset):

    if use_dataset == "HCP_retest":
        return "*/unprocessed/3T/T1w_MPR1/*_3T_T1w_MPR1"
    elif use_dataset == "HCP_1200":
        return "*/T1w/T1w_acpc_dc"
    elif use_dataset == "2022_QIM_52_Bone":
        #return "femur_*"
        return "resliced_femur*"
    else:
        return "*"


# If there is time, look at this post to see how the full dataset can be loaded.
# https://stackoverflow.com/questions/72012067/pytorch-dataloader-dataset-complete-in-ram

class VOXELDATA(torch.utils.data.Dataset):
    """
    Data class to load in and transform the data
    """

    def __init__(self, train, width, height, depth, _transform_lr, _transform_hr, transforms_list, data_path="PRIMO_BH_MULTIPLE_LABELS", train_path="train", test_path="test", combine_train_test=False):
        """
        This assumes the structure:
        PRIMO_BH_MULTIPLE_LABELS
            - test_images
            - train_images
            - train_labels
        :param train: Boolean to select if the test or train data should be loaded
        :param size: Image size to return
        :param data_path: Path to the PRIMO_BH_MULTIPLE_LABELS folder
        :param _transform: A torchvision transform object containing only non random transformations!
        """

        self._width = width
        self._height = height
        self._depth = depth
        self._transform_lr = _transform_lr
        self._transform_hr = _transform_hr
        self._transforms_list = transforms_list
        self._train = train
        self._root_dir = data_path

        if combine_train_test:
            self.data_path = os.path.join(self._root_dir, 'train_and_test_images')
        elif self._train:
            self.data_path = os.path.join(self._root_dir, train_path)
        else:
            self.data_path = os.path.join(self._root_dir, test_path)

        if DCSRN_config.use_dataset == "2022_QIM_52_Bone":
            self.image_paths = sorted(glob.glob(os.path.join(self.data_path, nvg_func(DCSRN_config.use_dataset))))
        else:
            self.image_paths = sorted(glob.glob(os.path.join(self.data_path, nvg_func(DCSRN_config.use_dataset) + DCSRN_config.IMAGE_FORMAT)))
        #self.mask_paths = sorted(glob.glob(os.path.join(self._root_dir, "train_labels", "*.png"))) if self._train else None
        #self._test_mask = torch.zeros(self._height, self._width)

    def __len__(self):
        """
        Returns the total number of samples
        :return: # of images
        """
        return len(self.image_paths)

    def transform(self, image):
        """
        The idea of this function is that you can add transformations. This can be done in multiple ways
        but here it is important that the exact same transformation is done to the training label and image
        In the example of the PRIMO images, each input image is cropped to 544x544, then downsampled to 272x272
        before being fed into the network. The same is done one each training label image.
        Something that could be added to improve stability would be to normalize the images.
        :param image: input image
        :param mask: label image, also called a mask.
        :return: Transformed images and masks
        """
        # Manual crop
        if 'manual_crop' in self._transforms_list:
            image = TF.crop(image, top=0, left=0, height=self._width, width=self._width)  # Crop to 544x544

        # Random crop
        if 'random_crop' in self._transforms_list:
            #random_crop = transforms.RandomCrop(size=(self._height, self._width))
            #image = random_crop(image)
            #lo_res = copy.deepcopy(hi_res)

            sampler = tio.data.UniformSampler(DCSRN_config.PATCH_SIZE_HR)
            # t1_image = tio.ScalarImage(image_path)
            image = list(sampler(image, 1))[0]
            image = image.one_image.data
        else:
            prop_tensor = torch.zeros((1, self._height, self._width, self._depth))
            prop_map = tio.Image(tensor=prop_tensor, type=tio.SAMPLING_MAP)
            image.add_image(prop_map, 'prop_map')
            ts = tio.ToCanonical(), tio.Resample('one_image')
            t = tio.Compose(ts)
            image = t(image)
            shape = image.prop_map.shape
            image.prop_map.data[0, int(shape[1]/2), int(shape[2]/2), int(shape[3]/2)] = 1.0  # Assign prop 1 at the center
            sampler = tio.data.WeightedSampler(DCSRN_config.PATCH_SIZE_HR, 'prop_map')
            # t1_image = tio.ScalarImage(image_path)
            image = list(sampler(image, 1))[0]
            image = image.one_image.data


        # Random horizontal flipping
        if 'hflip' in self._transforms_list:
            if self._train:
                if random.random() > 0.5:
                    image = TF.hflip(image)

        # Random vertical flipping
        if 'vflip' in self._transforms_list:
            if self._train:
                if random.random() > 0.5:
                    image = TF.vflip(image)

        """ Example on how you could add a transformation you define yourself
        # Random horizontal flipping
        if self._train:
            if random.random() > 0.5:
                image = TF.hflip(image)
                mask = TF.hflip(mask)
        """

        """
        tensor = transforms.ToTensor()
        # Normalize each channel
        if 'normalize_gray' in self._transforms_list:
            # Images are normalized if this function is enabled. Note that images should be unnormalized later
            # if this is enabled. More info can be found at:
            # https://www.geeksforgeeks.org/how-to-normalize-images-in-pytorch/

            #norm_rgb = transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
            norm_gray = transforms.Normalize(0.5, 0.5)
            image = norm_gray(tensor(image))
            #lo_res = norm_gray(tensor(lo_res))
        elif 'normalize_rgb' in self._transforms_list:
            norm_rgb = transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
            image = norm_rgb(tensor(image))
        """


        # Apply the transformations defined in the input transform parameter. Remember to end it with 'to_tensor'
        hi_res = self._transform_hr(image)

        # Gaussian smoothing
        if 'gaussian_blur' in self._transforms_list:
            if DCSRN_config.GAUSS_K_SIZE == None:
                #k_size = self._width // 25
                #if k_size % 2 == 0:
                #    k_size -= 1
                #hi_res_blur = TF.gaussian_blur(image, kernel_size=k_size)
                pass
            else:
                radius = np.ceil(3 * DCSRN_config.GAUSS_SIGMA)
                hi_res_blur = test_3d_gaussian_blur(image[0].float(), ks=int(2*radius+1), blur_sigma=DCSRN_config.GAUSS_SIGMA)
                #hi_res_blur = TF.gaussian_blur(image, kernel_size=DCSRN_config.GAUSS_K_SIZE, sigma=DCSRN_config.GAUSS_SIGMA)

            # Resize
            resize = tiotransforms.Resize(target_shape=(self._height // DCSRN_config.UP_FACTOR,
                                                        self._width // DCSRN_config.UP_FACTOR,
                                                        self._depth // DCSRN_config.UP_FACTOR),
                                                        image_interpolation='LINEAR')
            if DCSRN_config.UP_FACTOR == 1:
                lo_res = self._transform_lr(hi_res_blur)
            else:
                lo_res = self._transform_lr(resize(hi_res_blur))  # Only resize after all transforms to get low resolution image
        else:
            # Resize
            #resize = transforms.Resize(size=(self._height // 4, self._width // 4), interpolation=transforms.InterpolationMode.BILINEAR)  # Resize to 128x128, Default is Bilinear interpolation
            resize = tiotransforms.Resize(target_shape=(self._height // DCSRN_config.UP_FACTOR,
                                                        self._width // DCSRN_config.UP_FACTOR,
                                                        self._depth // DCSRN_config.UP_FACTOR),
                                                        image_interpolation='LINEAR')
            if DCSRN_config.UP_FACTOR == 1:
                lo_res = self._transform_lr(image)
            else:
                lo_res = self._transform_lr(resize(image))  # Only resize after all transforms to get low resolution image

        return hi_res, lo_res

    def _load_mask(self, idx):
        """
        Helper function to load in masks
        :param idx: index to return
        :return: mask with the given index
        """
        mask = Image.open(self.mask_paths[idx])
        mask = np.asarray(mask).copy()

        # Binarize masks ?
        #mask[mask > 0] = 255
        return Image.fromarray(mask)

    def __getitem__(self, idx):
        """
        This is the entire idea of this function and makes it a python generator function which is what Pytorch
        assumes for the dataloader
        :param idx: Image index to return
        :return: Return a X, Y pair of image and mask
        """
        if DCSRN_config.use_dataset == "2022_QIM_52_Bone":

            # if DCSRN_config.PRELOAD_BONE_DATA or DCSRN_config.PRELOAD_BONE_DATA_TEST:
            #
            #     ## ASSUMES PRELOADED SUBJECTS
            #     subject = DCSRN_config.SUBJECTS_DATASET[idx]
            #     #subject_patch = list(DCSRN_config.SAMPLER(subject, 1))[0]
            #     subject_patch = next(iter(DCSRN_config.SAMPLER(subject)))
            #     #subject_patch = list(DCSRN_config.SAMPLER(DCSRN_config.SUBJECTS_LIST[idx], 1))[0]
            #     if DCSRN_config.UP_FACTOR != 1:
            #         lo_res = DCSRN_config.DOWNSCALE_FUNC(subject_patch['lo_res']).data
            #     hi_res = subject_patch['hi_res'].data
            #
            #     # Normalize based on the whole scan
            #     lo_res = (lo_res - subject_patch['mean_lo_res']) / subject_patch['std_lo_res']
            #     hi_res = (hi_res - subject_patch['mean_hi_res']) / subject_patch['std_hi_res']
            #     return hi_res, lo_res

            #image_path_hr = os.path.join(self.image_paths[idx], "/DICOM/hi_res")
            #image_path_lr = os.path.join(self.image_paths[idx], "/DICOM/lo_res")
            hr_patch, lr_patch_downscaled = load_patch_subjects_V3(data_path=self.image_paths[idx])

            return hr_patch.one_image.data.float(), lr_patch_downscaled.one_image.data.float()

        else:
            image_path = self.image_paths[idx]
            subject = tio.Subject(one_image=tio.ScalarImage(image_path))

            hi_res, lo_res = self.transform(subject)

            return hi_res, lo_res


class CUSTOM_QUEUE(torch.utils.data.Dataset):

    def __init__(self, subjects_dataset, queue_length, samples_per_volume, sampler, n_workers, shuffle_sub, shuffle_pat, subject_sampler):

        self.subjects_dataset = subjects_dataset
        self.queue = tio.Queue(
        subjects_dataset=subjects_dataset,
        max_length=queue_length,
        samples_per_volume=samples_per_volume,
        sampler=sampler,
        num_workers=n_workers,
        shuffle_subjects=shuffle_sub,
        shuffle_patches=shuffle_pat,
        verbose=True,
        start_background=True,
        subject_sampler=subject_sampler
        )
        #self.Resize = tiotransforms.Resize(target_shape=(80 // 4), image_interpolation='LINEAR')

    def __len__(self):
        """
        Returns the total number of samples
        :return: # of images
        """
        #return len(self.subjects_dataset)
        return len(self.queue)

    def __getitem__(self, idx):
        try:
            subject_patch = next(iter(self.queue))
        except ValueError:
            print("Value error encountered, fetching patch again...")
            subject_patch = next(iter(self.queue))
        #print("Patch extracted from: " + subject_patch['name'])

        #hi_res = patch_batch['hi_res']['data']
        #lo_res = patch_batch['lo_res']['data']


        #lo_res = self.Resize(subject_patch['lo_res']).data
        #if DCSRN_config.UP_FACTOR != 1:
        #    if DCSRN_config.GAUSS_SIGMA > 0:
        #        radius = np.ceil(3 * DCSRN_config.GAUSS_SIGMA)
        #        hi_res_blur = test_3d_gaussian_blur(subject_patch['lo_res'].data[0].float(), ks=int(2 * radius + 1),
        #                                            blur_sigma=DCSRN_config.GAUSS_SIGMA)
        #        lo_res = DCSRN_config.DOWNSCALE_FUNC(hi_res_blur).data
        #    else:
        #        lo_res = DCSRN_config.DOWNSCALE_FUNC(subject_patch['lo_res']).data

        if DCSRN_config.GAUSS_SIGMA > 0:
            radius = np.ceil(3 * DCSRN_config.GAUSS_SIGMA)
            hi_res_blur = test_3d_gaussian_blur(subject_patch['lo_res'].data[0].float(), ks=int(2 * radius + 1),
                                                blur_sigma=DCSRN_config.GAUSS_SIGMA)
            if DCSRN_config.UP_FACTOR == 1:
                lo_res = hi_res_blur.data
            else:
                lo_res = DCSRN_config.DOWNSCALE_FUNC(hi_res_blur).data
        else:
            if DCSRN_config.UP_FACTOR == 1:
                lo_res = subject_patch['lo_res'].data
            else:
                lo_res = DCSRN_config.DOWNSCALE_FUNC(subject_patch['lo_res']).data

        hi_res = subject_patch['hi_res'].data

        # Normalize based on the whole scan
        lo_res = (lo_res - subject_patch['mean_lo_res']) / subject_patch['std_lo_res']
        hi_res = (hi_res - subject_patch['mean_hi_res']) / subject_patch['std_hi_res']
        return hi_res, lo_res

