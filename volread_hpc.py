"""
Created on Tue Oct 13 2020

@author: Anders Bjorholm Dahl
abda@dtu.dk

Edited on Mon Apr 24 2023
@author: August Leander Høeg
s173944@student.dtu.dk

"""
import numpy as np
import io
import torchio as tio
import os
import glob
import torch
import nibabel as nib
import re

def savenii(subject_hr, data_path, file_name):

    #raw_image_hr = subject_hr['one_image']['data']  #
    
    subject_hr['one_image'].save(os.path.join(data_path, file_name))
    
    


def readvol(file_name, expand_first_dim = False):
    """
    Todo: Test this function on images of uint8, uint16, and when dimensions are
    not all the same.

    Parameters
    ----------
    file_name : Sting
        name of .vol and .vgi files without extension

    Returns
    -------
    V : numpy array (float32, uint8, or uint16)
        3D volume.

    """
    vgi_name = file_name + '.vgi'
    f = io.open(vgi_name, 'r')
    t = f.read()
    f.close()
    elem = []
    dt = ''
    header_dict = {
        "pixdim": [],
        "dt": None,
        "datarange" : [],
        "position" : [],
    }
    resolution_instance_no = 0
    datarange_instance_no = 0
    position_instance_no = 0

    for l in t.splitlines():
        if(l.startswith('Size = ')):
            elem = [int(n) for n in l.split() if n.isdigit()]
        if(l.startswith('Datatype = float')):
            dt = np.float32
        elif(l.startswith('Datatype = uint8')):
            dt = np.uint8
        elif(l.startswith('Datatype = unsigned integer')):
            dt = np.uint16
        elif(l.startswith('resolution = ')):
            if(resolution_instance_no == 0):
                pixdim = re.findall("\d+\.\d+", l)
                pixdim = [float(ele) for ele in pixdim]
                header_dict['pixdim'] = pixdim
            resolution_instance_no += 1

        elif(l.startswith('position = ')):
            if(position_instance_no == 0):
                position = re.findall(r"[-+]?(?:\d*\.*\d+)", l)
                position = [float(ele) for ele in position]
                header_dict['position'] = position
            position_instance_no += 1

        elif(l.startswith('datarange =')):
            if(datarange_instance_no == 1):
                datarange = re.findall(r"[-+]?(?:\d*\.*\d+)", l)
                datarange = [float(ele) for ele in datarange]
                header_dict['datarange'] = datarange
            datarange_instance_no += 1

    header_dict['dt'] = dt

    vol_name = file_name + '.vol'
    V = np.fromfile(vol_name, dtype = dt, count = elem[0]*elem[1]*elem[2])
    V = np.reshape(V, (elem[2], elem[0], elem[1]))
    V = V.transpose((0,2,1))

    if expand_first_dim == True:
        V = np.expand_dims(V, axis=0)

    return V, header_dict

if __name__ == "__main__":

	
    #dataset_path = "/dtu/3d-imaging-center/projects/2022_QIM_52_Bone/raw_data_3DIM"
    dataset_path = "/dtu/3d-imaging-center/projects/2022_QIM_52_Bone/raw_data_3DIM"
    
    #test_path = "femur_15 1 [2022-02-25 09.16.41]/femur_15 1_reco"
    #test_path = "femur_15 2 [2022-02-25 11.27.58]/femur_15_2_reco"
    
    #test_path = "femur_21 1 [2022-03-10 10.24.02]/femur_21 1_reco"
    #test_path = "femur_21 2 [2022-03-10 12.35.19]/femur_21 2_reco"
    
    test_path = "femur_74_c 1 [2022-03-29 14.35.22]/femur_74_c 1_reco"
    #test_path = "femur_74_c 2 [2022-03-29 16.46.40]/femur_74_c 2_reco"
    
    #test_path = "femur_01_b 1 [2022-03-09 10.52.18]/femur_01_b 1_recon"
    #test_path = "femur_01_b 2 [2022-03-09 13.03.36]/femur_01_b 2_reco"

    data_path = os.path.join(dataset_path, test_path)

    #vol_file_name = "femur_01_b 1"
    #vol_file_name = "femur_01_b 2"
    
    #vol_file_name = "femur_15 1"
    #vol_file_name = "femur_15 2"
    
    #vol_file_name = "femur_21 1"
    #vol_file_name = "femur_21 2"
    
    vol_file_name = "femur_74_c 1"
    #vol_file_name = "femur_74_c 2"    
    
    new_dtype = np.int16
    #new_dtype = None

    if vol_file_name == "femur_15 1":  # WORKS BUT A LITTLE OFF
        x_crop = [0, 1950]
        y_crop = [450, 1700]
        z_crop = [100, 1700]
    elif vol_file_name == "femur_21 1":  # WORKS
        x_crop = [100, 1900]
        y_crop = [100, 1700]
        z_crop = [400, 1800]
    elif vol_file_name == "femur_74_c 1": # WORKS
        x_crop = [100, 1800]
        y_crop = [200, 1800]
        z_crop = [400, 1800]
    elif vol_file_name == "femur_01_b 1":
        x_crop = [100, 1850]
        y_crop = [0, 1400]
        z_crop = [100, 1700]

    vol_array, header_dict = readvol(os.path.join(data_path, vol_file_name))

    file_name = vol_file_name + "_int16.nii"
    if False:
        vol_array = torch.from_numpy(vol_array)
        subject_hr = tio.Subject(one_image=tio.ScalarImage(tensor=vol_array))

        savenii(subject_hr, data_path, file_name)
    else:
        if new_dtype is not None:
            data_type = new_dtype
            header_dict["dt"] = new_dtype
            vol_array = vol_array.astype(data_type)

        if x_crop is not None:
            vol_array = vol_array[x_crop[0]:x_crop[1], y_crop[0]:y_crop[1], z_crop[0]:z_crop[1]]
            file_name = "cropped_" + file_name

        ni_img = nib.Nifti1Image(vol_array, affine=np.eye(4))
        #ni_img.header["sform_code"] = 0  # UNKNOWN ALIGNMENT, SHOULD BE USED FOR DICOM GENERATION ONLY
        #ni_img.header.set_sform(np.diag([0, 0, 0, 0]))  # SET zero matrix sform, SHOULD BE USED FOR DICOM GENERATION ONLY
        print(np.array(header_dict["pixdim"]))
        ni_img.header["pixdim"][1:4] = np.array(header_dict["pixdim"])
        save_path = "/work3/s173944/Python/venv_srgan/3D_datasets/datasets/2022_QIM_52_Bone/train/raw"
        nib.save(ni_img, os.path.join(save_path, file_name))

    if False:
        vol_tensor = torch.from_numpy(vol_array)
        W, H, D = vol_tensor.shape
        resize = tiotransforms.Resize(target_shape=(W // 4, H // 4, D // 4),
                                      image_interpolation='LINEAR')
        vol_tensor_lo_res = resize(vol_tensor)

        vol_array_lo_res = vol_tensor_lo_res.numpy()




