# Investigating the impact of auxiliary loss functions for super-resolution using GANs

## Description
This repository contains the source code written as part of the Master Thesis "Investigating the impact of auxiliary loss functions for super-resolution using GANs", by August Leander Høeg.

Super-resolution samples from the validation set of the Human Connectome Project (HCP) 1200 dataset can be seen below. From left to right: Nearest-neighbour interpolation, 3D cubic B-spline interpolation, GAN model and High resolution ground truth.

![image info](figures/slice_00_comp_X.png)

![image info](figures/slice_01_comp_Y.png)

![image info](figures/slice_02_comp_X.png)



