import torch
import torch.nn as nn
from torchvision.models import vgg19

import DCSRN_config
import SRGAN_config
#from kornia.filters import spatial_gradient3d


if __name__ == "__main__":

    ##### From mDCSRN-GAN paper #####
    # Quality Metrics. To quantitatively measure mDCSRN’s recovery accuracy, we used
    # three reference-based image similarity metrics: structural similarity index (SSIM) [45],
    # peak signal to noise ratio (PSNR), and normalized root mean squared error (NRMSE).
    # Numbers were calculated in the most resolution degraded cross-section (2 × 2) slice by
    # slice. Scores were reported in its subject-wise slice-averaged numbers. For mDCSRN-
    # GAN measurement, we list its numeric metrics as well. But we need to point out that
    # PSNR could not fully represent the visual quality. Hence, we measured the perceptual
    # quality via non-reference metrics: PIQE [46], Ma’s score [47], NIQE [48], and perceptual
    # index (PI, used in PRIM-SR Challenge [49]). To efficiently calculate the perceptual
    # scores, we only processed the 2D slices where the foreground (brain region) occupies
    # more than 25% of the whole image. All perceptual scores were calculated in MATLAB
    # R2019 software.

    print("Done")