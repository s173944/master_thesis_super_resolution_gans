import copy

import matplotlib
import numpy as np
import math
import torch
import torch.nn.functional as F
import DCSRN_config
import st3d
import utils
from st3d import eig_special_3d, structure_tensor_3d
import matplotlib.pyplot as plt
import torchio as tio

from utils import gauss_dev_1D

def normalize_S(S_ele, tensor=False):
    a = S_ele[0]
    d = S_ele[1]
    f = S_ele[2]
    b = S_ele[3]
    c = S_ele[4]
    e = S_ele[5]
    determinant_S = a*(d*f-e**2) + b*(c*e-b*f) + c*(b*e-d*c)
    if tensor:
        S_ele = S_ele / torch.sqrt(determinant_S)
    else:
        S_ele = S_ele / np.sqrt(determinant_S)
    return S_ele

def reduce_3D(m):
    mR1 = np.concatenate([m[0,0], m[0,1], m[0,2]], 1)
    mR2 = np.concatenate([m[1,0], m[1,1], m[1,2]], 1)
    mR3 = np.concatenate([m[2,0], m[2,1], m[2,2]], 1)
    return np.concatenate([mR1, mR2, mR3], 2)

def convert_3x3_st(S_ele, tensor=False):
    size = S_ele.shape[-1]
    if tensor:
        if S_ele.device.type == "cuda":
            device = DCSRN_config.DEVICE
        else:
            device = torch.device('cpu')
        S_full = torch.zeros((3, 3, size, size, size), device=device)
    else:
        S_full = np.zeros((3, 3, size, size, size))
    S_full[0, 0] = S_ele[0]
    S_full[1, 1] = S_ele[1]
    S_full[2, 2] = S_ele[2]
    S_full[0, 1] = S_ele[3]
    S_full[1, 0] = S_ele[3]
    S_full[0, 2] = S_ele[4]
    S_full[2, 0] = S_ele[4]
    S_full[1, 2] = S_ele[5]
    S_full[2, 1] = S_ele[5]
    return S_full
#S_flat = S[[0, 1, 2, 0, 0, 1], [0, 1, 2, 1, 2, 2]]  # Reshaping S as st needs

def matmul_3x3(A,B, transpose_left=False, tensor=False): # Assumes A and B are both 3x3
    size = max(max(A.shape), max(B.shape))
    if tensor:
        if (A.device.type == "cuda") or (B.device.type == "cuda"):
            device = DCSRN_config.DEVICE
        else:
            device = torch.device('cpu')
        result = torch.zeros((3,3,size,size,size), device=device)
    else:
        result = np.zeros((3, 3, size, size, size))
    for i in range(3):
        for j in range(3):
            sum = 0.0
            for k in range(3):
                if transpose_left:  # Swap rows and columns of left matrix
                    sum += A[k,i] * B[k,j]
                else:
                    sum += A[i,k] * B[k,j]
            result[i,j] = sum
    return result

def st_principal_log(S_ele, test_decomp=False):
    val, vec = eig_special_3d(S_ele, full=True)
    if len(S_ele.shape) > 1:
        size = S_ele[0].shape[-1]
    else:
        size = 1
    if test_decomp:
        val_full = np.zeros((3, 3, size, size, size))
        val_full[0, 0] = val[0]
        val_full[1, 1] = val[1]
        val_full[2, 2] = val[2]
        S = convert_3x3_st(S_ele)
        tmp = matmul_3x3(np.transpose(vec, axes=(1, 0, 2, 3, 4)), val_full)
        print("Summed difference of S - R'*D*R =", np.sum(matmul_3x3(tmp, vec) - S))

    # Compute natural logarithm of eigenvalues:
    min_tol = 1e-27
    log_val = np.log(np.clip(val, a_min=min_tol, a_max=None))
    if size == 1:
        D_tilde = np.diag(log_val)
        log_S = vec.T @ D_tilde @ vec
    else:
        D_tilde = np.zeros((3, 3, size, size, size))
        D_tilde[0, 0] = log_val[0]
        D_tilde[1, 1] = log_val[1]
        D_tilde[2, 2] = log_val[2]
        tmp = matmul_3x3(np.transpose(vec, axes=(1, 0, 2, 3, 4)), D_tilde)
        log_S = matmul_3x3(tmp, vec)  # OKAY THIS FUCKING WORKS!!!
    return log_S

def st_principal_log_torch(S_ele, test_decomp=False):
    if S_ele.device.type == "cuda":
        device = DCSRN_config.DEVICE
    else:
        device = torch.device('cpu')

    val, vec = eig_special_3d_torch(S_ele, full=True)
    if len(S_ele.shape) > 1:
        size = S_ele[0].shape[-1]
    else:
        size = 1
    if test_decomp:
        val_full = torch.zeros((3, 3, size, size, size), device=device)
        val_full[0, 0] = val[0]
        val_full[1, 1] = val[1]
        val_full[2, 2] = val[2]
        S = convert_3x3_st(S_ele, tensor=True)
        tmp = matmul_3x3(torch.transpose(vec, dim0=0, dim1=1), val_full, tensor=True)
        print("Summed difference of S - R'*D*R =", torch.sum(matmul_3x3(tmp, vec, tensor=True) - S))

    # Compute natural logarithm of eigenvalues:
    min_tol = 1e-8
    log_val = torch.log(torch.clip(val, min=min_tol, max=None))
    if size == 1:
        D_tilde = torch.diag(log_val)
        log_S = vec.T @ D_tilde @ vec
    else:
        D_tilde = torch.zeros((3, 3, size, size, size), device=device)
        D_tilde[0, 0] = log_val[0]
        D_tilde[1, 1] = log_val[1]
        D_tilde[2, 2] = log_val[2]
        tmp = matmul_3x3(torch.transpose(vec, dim0=0, dim1=1), D_tilde, tensor=True)
        log_S = matmul_3x3(tmp, vec, tensor=True)  # OKAY THIS FUCKING WORKS!!!
    return log_S

def log_euclidean_metric(S1_ele, S2_ele, method="frobenius"):

    log_S1 = st_principal_log(S1_ele)
    log_S2 = st_principal_log(S2_ele)
    if method == "simple":
        log_diff = log_S1 - log_S2
        a = log_diff*log_diff  # square the difference
        trace = np.sum(a[0,0] + a[1,1] + a[2,2])  # Compute the trace, then sum all the (n,n,n) elements
        dist = np.sqrt(trace)
    elif method == "frobenius":
        M = log_S1 - log_S2
        a = matmul_3x3(np.transpose(M,axes=(1,0,2,3,4)), M)
        trace = a[0, 0, :, :, :] + a[1, 1, :, :, :] + a[2, 2, :, :, :]
        dist_points = np.sqrt(trace)
        dist = np.sum(dist_points)
    return dist

def log_euclidean_metric_torch(S1_ele, S2_ele, method="frobenius"):
    if False:
        S1_ele = S1_ele.double()
        S2_ele = S2_ele.double()

    log_S1 = st_principal_log_torch(S1_ele)
    log_S2 = st_principal_log_torch(S2_ele)
    if method == "simple":
        log_diff = log_S1 - log_S2
        a = log_diff*log_diff  # square the difference
        trace = torch.sum(a[0,0] + a[1,1] + a[2,2])  # Compute the trace, then sum all the (n,n,n) elements
        dist = torch.sqrt(trace)
    elif method == "frobenius":
        M = log_S1 - log_S2
        a = matmul_3x3(torch.transpose(M,dim0=0,dim1=1), M, tensor=True)
        trace = a[0, 0, :, :, :] + a[1, 1, :, :, :] + a[2, 2, :, :, :]
        dist_points = torch.sqrt(trace)
        dist = torch.sum(dist_points)
    return dist, dist_points


def log_euclidean_metric_point(S1, S2, method="frobenius"):

    log_S1 = st_principal_log(S1)
    log_S2 = st_principal_log(S2)
    log_S1_flat = log_S1[[0, 1, 2, 0, 0, 1], [0, 1, 2, 1, 2, 2]]
    log_S2_flat = log_S2[[0, 1, 2, 0, 0, 1], [0, 1, 2, 1, 2, 2]]
    if method == "simple":
        log_diff = log_S1_flat - log_S2_flat
        a = log_diff**2  # square the difference
        trace = a[0] + a[1] + a[2]  # Compute the trace, then sum all the (n,n,n) elements
        dist = np.sqrt(trace)
    elif method == "frobenius": # || A ||_F = sqrt(trace(A'*A))
        M = log_S1 - log_S2
        a = M.T @ M
        trace = np.trace(a)
        dist = np.sqrt(trace)
    return dist

def convert_1x6_st(S_full, tensor=False):
    size = max(S_full.shape)
    if tensor:
        if S_full.device.type == "cuda":
            device = DCSRN_config.DEVICE
        else:
            device = torch.device('cpu')
        S_ele = torch.zeros((6, size, size, size), device=device)
    else:
        S_ele = np.zeros((6, size, size, size))
    S_ele[0] = S_full[0, 0]  # xx
    S_ele[1] = S_full[1, 1]  # yy
    S_ele[2] = S_full[2, 2]  # zz
    S_ele[3] = S_full[0, 1]  # xy
    S_ele[4] = S_full[0, 2]  # xz
    S_ele[5] = S_full[1, 2]  # yz
    return S_ele


def rotation3D(theta_deg, phi_deg, psi_deg, as_tensor=False):
    theta = (theta_deg / 180) * np.pi
    phi = (phi_deg / 180) * np.pi
    psi = (psi_deg / 180) * np.pi

    Rx = np.array([[1, 0, 0], [0, np.cos(phi), -np.sin(phi)], [0, np.sin(phi), np.cos(phi)]])
    Ry = np.array([[np.cos(psi), 0, np.sin(psi)], [0, 1, 0], [-np.sin(psi), 0, np.cos(psi)]])
    Rz = np.array([[np.cos(theta), -np.sin(theta), 0], [np.sin(theta), np.cos(theta), 0], [0, 0, 1]])

    Rxyz = Rx @ Ry @ Rz
    if as_tensor:
        Rxyz = torch.tensor(Rxyz)
    return Rxyz


def eig_special_3d_torch(S, full=False):
    """Eigensolution for symmetric real 3-by-3 matrices.

    Arguments:
        S: ndarray
            A floating point array with shape (6, ...) containing structure tensor.
            Use float64 to avoid numerical errors. When using lower precision, ensure
            that the values of S are not very small/large.
        full: bool, optional
            A flag indicating that all three eigenvalues should be returned.

    Returns:
        val: ndarray
            An array with shape (3, ...) containing sorted eigenvalues
        vec: ndarray
            An array with shape (3, ...) containing eigenvector corresponding to
            the smallest eigenvalue. If full, vec has shape (3, 3, ...) and contains
            all three eigenvectors.

    More:
        An analytic solution of eigenvalue problem for real symmetric matrix,
        using an affine transformation and a trigonometric solution of third
        order polynomial. See https://en.wikipedia.org/wiki/Eigenvalue_algorithm
        which refers to Smith's algorithm https://dl.acm.org/citation.cfm?id=366316.

    Authors: vand@dtu.dk, 2019; niejep@dtu.dk, 2019-2020
    """

    #S = torch.tensor(S)
    #S = S.clone().detach().requires_grad_(True)

    if S.device.type == "cuda":
        device = DCSRN_config.DEVICE
    else:
        device = torch.device('cpu')

    # Check data type. Must be floating point 64.
    #if not S.dtype == torch.float64:
    #    S = S.double()
    if not S.dtype == torch.float32:
        if S.dtype == torch.float64:
            pass
        else:
            S = S.float()
        #raise ValueError('S must be floating point type.')

    # Flatten S.
    input_shape = S.shape
    S = S.reshape(6, -1)
    N = max(S.shape)

    # Create v vector.
    v = torch.tensor([[2 * np.pi / 3], [4 * np.pi / 3]], dtype=S.dtype, device=device)

    # Computing eigenvalues.

    # Allocate vec and val. We will use them for intermediate computations as well.
    if full:
        val = torch.empty((3, N), dtype=S.dtype, device=device)
        vec = torch.empty((9, N), dtype=S.dtype, device=device)
        tmp = torch.empty((4, N), dtype=S.dtype, device=device)
        B03 = val
        B36 = vec[:3]
    else:
        val = torch.empty((3, ) + S.shape[1:], dtype=S.dtype, device=device)
        vec = torch.empty((3, ) + S.shape[1:], dtype=S.dtype, device=device)
        tmp = torch.empty((4, ) + S.shape[1:], dtype=S.dtype, device=device)
        B03 = val
        B36 = vec

    S1 = S[0]
    S2 = S[1]
    S3 = S[2]
    S4 = S[3]
    S5 = S[4]
    S6 = S[5]
    S_first3 = S[:3]
    S_last3 = S[3:]
    # Compute q, mean of diagonal. We need to use q multiple times later.
    # Using np.mean has precision issues.
    #q = torch.add(S[0], S[1], out=tmp[0])
    q = torch.add(S1, S2)
    #tmp[0] = q.clone()
    q = q + S3
    q = q / 3

    # Compute S minus q. Insert it directly into B where it'll stay.
    #Sq = torch.subtract(S[:3], q, out=B03)
    Sq = torch.subtract(S_first3, q)
    B03 = Sq.clone()

    # Compute s, off-diagonal elements. Store in part of B not yet used.
    s = torch.einsum('ij,ij->j', S_last3, S_last3)
    #tmp[1] = s.clone()  # Maybe we should use s.clone()
    s = s * 2

    # Compute p.
    p = torch.einsum('ij,ij->j', Sq, Sq)
    #tmp[2] = p.clone()  # Maybe we should use p.clone()
    #del Sq  # Last use of Sq.
    p = p + s

    p = p*(1/6)
    #torch.sqrt(p, out=p)
    p = torch.sqrt(p)

    # Compute inverse p, while avoiding 0 division.
    # Reuse s allocation and delete s to ensure we don't efter it's been reused.
    #del s
    p_inv = torch.zeros_like(p)
    #p_inv = s
    #del s
    #p_inv[:] = 0
    #np.divide(1, p, out=p_inv, where=p != 0)
    min_tol = 1e-27  # 1e-8 works for smaller patches
    method = "clamp"
    if method == "clamp":
        p = torch.clamp(p, min=min_tol, max=None)
        p_inv = torch.divide(1, p)
    else:
        # Only divide 1 by the values of p that are over min_tol.
        # This should ensure that the values of p_inv do not blow up when p is close to zero.
        p_inv = torch.divide((p < min_tol), p)
    #p_inv[p != 0] = torch.divide(1, p[p != 0].clone())  ##  How do we make this out-of-place?

    # Compute B. First part is already filled.
    B03 = B03 * p_inv
    #torch.multiply(S[3:], p_inv, out=B36)
    B36 = torch.multiply(S_last3, p_inv)

    # Compute d, determinant of B.
    d = torch.prod(B03, dim=0)
    #tmp[3] = d.clone()

    # Views for B.
    B0 = B03[0]
    B1 = B03[1]
    B2 = B03[2]
    B3 = B36[0]
    B4 = B36[1]
    B5 = B36[2]
    # Reuse allocation for p_inv and delete variable.
    d_tmp = p_inv
    #del p_inv
    # Computation of d.
    d_tmp = torch.multiply(B2, B3)
    d_tmp = d_tmp * B3
    #d_tmp = torch.multiply(B03[2], B36[0])
    #d_tmp = d_tmp * B36[0]
    d = d - d_tmp
    #torch.multiply(B4, B4, out=d_tmp)
    d_tmp = torch.multiply(B4, B4)
    d_tmp = d_tmp * B1
    #d_tmp = torch.multiply(B36[1], B36[1])
    #d_tmp = d_tmp * B03[1]
    d = d - d_tmp
    d_tmp = torch.prod(B36, dim=0)
    d_tmp = d_tmp * 2
    d = d + d_tmp
    d_tmp = torch.multiply(B5, B5)
    d_tmp = d_tmp * B0
    #d_tmp = torch.multiply(B36[2], B36[2])
    #d_tmp = d_tmp * B03[0]
    d = d - d_tmp
    d = d * 0.5
    # Ensure -1 <= d/2 <= 1.
    #torch.clip(d, -1, 1, out=d)
    d = torch.clip(d, -1, 1)

    # Compute phi. Beware that we reuse d variable!
    phi = d
    #phi = torch.arccos(phi, out=phi)
    phi = torch.arccos(phi)
    phi = phi / 3

    # Compute val, ordered eigenvalues. Resuing B allocation.
    #del B03, B36, B0, B1, B2, B3, B4, B5

    #np.add(v, phi[np.newaxis], out=val[:2])
    #torch.add(v, phi[None], out=val[:2])  # prepend dimension on phi
    #val[:2] = torch.add(v, phi[None].clone())  # this needs to be made out-of-place
    #val[2] = phi
    phi_singleton = phi[None]
    #val_A = torch.add(v, phi_singleton)
    val = torch.cat((torch.add(v, phi_singleton), phi_singleton), dim=0)

    #torch.cos(val, out=val)
    val = torch.cos(val)
    p = p * 2
    val = val * p
    val = val + q

    # Remove all variable using tmp allocation.
    #del q, p, phi, d_tmp

    # Computing eigenvectors -- either only one or all three.
    if full:
        l = val
        #vec = vec.reshape(3, 3, -1)
        #vec_tmp = tmp[:3]
    else:
        l = val[0]
        vec_tmp = tmp[2]

    # Compute vec. The tmp variable can be reused.

    # u = S[4] * S[5] - (S[2] - l) * S[3]
    #u = torch.subtract(S[2], l, out=vec[0])
    #torch.multiply(u, S[3], out=u)
    #u_tmp = torch.multiply(S[4], S[5], out=tmp[3])
    #torch.subtract(u_tmp, u, out=u)
    u = torch.subtract(S3, l)
    #vec[0] = u.clone()
    u = torch.multiply(u, S4)
    u_tmp = torch.multiply(S5, S6)
    #tmp[3] = u_tmp.clone()
    u = torch.subtract(u_tmp, u)
    # Put values of u into vector 2 aswell.

    ## v = S[3] * S[5] - (S[1] - l) * S[4]
    #v = torch.subtract(S[1], l, out=vec_tmp)
    #torch.multiply(v, S[4], out=v)
    #v_tmp = torch.multiply(S[3], S[5], out=tmp[3])
    #torch.subtract(v_tmp, v, out=v)
    v = torch.subtract(S2, l)
    #vec_tmp = v.clone()
    v = torch.multiply(v, S5)
    v_tmp = torch.multiply(S4, S6)
    #tmp[3] = v_tmp.clone()
    v = torch.subtract(v_tmp, v)

    ## w = S[3] * S[4] - (S[0] - l) * S[5]
    #w = torch.subtract(S[0], l, out=vec[2])
    #torch.multiply(w, S[5], out=w)
    #w_tmp = torch.multiply(S[3], S[4], out=tmp[3])
    #torch.subtract(w_tmp, w, out=w)
    w = torch.subtract(S1, l)
    #vec[2] = w.clone()
    w = torch.multiply(w, S6)
    w_tmp = torch.multiply(S4, S5)
    #tmp[3] = w_tmp.clone()
    w = torch.subtract(w_tmp, w)

    #vec[1] = u
    #torch.multiply(u, v, out=vec[0])
    vec0 = torch.multiply(u, v)
    #u = vec[1]
    # torch.multiply(u, w, out=vec[1])
    vec1 = torch.multiply(u, w)
    #torch.multiply(v, w, out=vec[2])
    vec2 = torch.multiply(v, w)
    #vec = torch.cat((vec0.unsqueeze(0), vec1.unsqueeze(0), vec2.unsqueeze(0)), dim=0)
    vec = torch.stack((vec0, vec1, vec2), dim=0)

    # Remove u, v, w and l variables.
    #del u, v, w, l

    # Normalizing -- depends on number of vectors.
    if full:
        # vec is [x1 x2 x3, y1 y2 y3, z1 z2 z3]
        #l = np.einsum('ijk,ijk->jk', vec, vec, out=vec_tmp)[:, np.newaxis]
        vec_tmp = torch.einsum('ijk,ijk->jk', vec, vec)
        l = vec_tmp[:, None]
        vec = torch.swapaxes(vec, 0, 1)
    else:
        # vec is [x1 y1 z1] = v1
        #l = np.einsum('ij,ij->j', vec, vec, out=vec_tmp)
        l = torch.einsum('ij,ij->j', vec, vec)
        vec_tmp = l

    #torch.sqrt(l, out=l)
    method = "sqrt_clamp"
    l_min = 1e-27  # 1e-25 produces no nan values
    if method == "clamp_sqrt":
        l = torch.clamp(l, min=l_min)
        l = torch.sqrt(l)
    elif method == "sqrt_clamp":
        l = torch.sqrt(l)
        l = torch.clamp(l, min=l_min)
    else:
        l = torch.sqrt(l)

    vec = vec / l

    val_start = val.shape[:-1]
    vec_start = vec.shape[:-1]
    input_end = input_shape[1:]
    return val.reshape(val_start + input_end), vec.reshape(vec_start + input_end)


















def get_gaussian_kernel(sigma, also_dg=False, also_ddg=False, radius=None):
    # order only 0 or 1

    if radius is None:
        radius = max(int(4 * sigma + 0.5), 1)  # similar to scipy _gaussian_kernel1d but never smaller than 1
    x = torch.arange(-radius, radius + 1)

    sigma2 = sigma * sigma
    phi_x = torch.exp(-0.5 / sigma2 * x ** 2)
    phi_x = phi_x / phi_x.sum()

    if also_dg:
        return phi_x, phi_x * -x / sigma2
    elif also_ddg:
        return phi_x, phi_x * -x / sigma2, phi_x * ((x**2/sigma2**2) - (1/sigma2))
    else:
        return phi_x

def structure_tensor(im, sigma, rho):
    '''
    Image is (1,H,W) torch tensor, st is (3,H,W) torch tensor.
    '''
    g, dg = get_gaussian_kernel(sigma, also_dg=True)
    h = (1, 1, -1, 1)
    w = (1, 1, 1, -1)
    Ix = torch.nn.functional.conv2d(im.unsqueeze(0), dg.reshape(h), padding='valid')
    Ix = torch.nn.functional.conv2d(Ix, g.reshape(w), padding='valid')
    Iy = torch.nn.functional.conv2d(im.unsqueeze(0), g.reshape(h), padding='valid')
    Iy = torch.nn.functional.conv2d(Iy, dg.reshape(w), padding='valid')

    k = get_gaussian_kernel(rho)
    Jxx = torch.nn.functional.conv2d(Ix ** 2, k.reshape(h), padding='same')
    Jxx = torch.nn.functional.conv2d(Jxx, k.reshape(w), padding='same')
    Jyy = torch.nn.functional.conv2d(Iy ** 2, k.reshape(h), padding='same')
    Jyy = torch.nn.functional.conv2d(Jyy, k.reshape(w), padding='same')
    Jxy = torch.nn.functional.conv2d(Ix * Iy, k.reshape(h), padding='same')
    Jxy = torch.nn.functional.conv2d(Jxy, k.reshape(w), padding='same')

    S = torch.cat((Jxx.squeeze(0), Jyy.squeeze(0), Jxy.squeeze(0)), dim=0)
    return S

def test_3d_gaussian_blur(vol, ks, blur_sigma):

    t = blur_sigma**2
    radius = math.ceil(3 * blur_sigma)
    x = np.arange(-radius, radius + 1)
    k = torch.from_numpy(1.0 / np.sqrt(2.0 * np.pi * t) * np.exp((-x * x) / (2.0 * t))).float()
    # k = torch.from_numpy(cv.getGaussianKernel(ks, blur_sigma)).squeeze().float()

    # Separable 1D convolution in 3 directions
    k1d = k.view(1, 1, -1)
    if vol.device.type == "cuda":
        k1d = k1d.to(DCSRN_config.DEVICE)
    for _ in range(3):
        vol = F.conv1d(vol.reshape(-1, 1, vol.size(2)), k1d, padding=ks // 2).view(*vol.shape)
        vol = vol.permute(2, 0, 1)
    #print((vol_3d- vol).abs().max()) # something ~1e-7
    #print(torch.allclose(vol_3d, vol, atol=1e-6))

    #vol = vol.reshape(1, *vol.shape)  # If you want to add singleton first dimension
    return vol

def test(vol, blur_sigma):
    # 3D convolution
    vol_in = vol.reshape(1, 1, *vol.shape)
    t = blur_sigma ** 2
    radius = math.ceil(3 * blur_sigma)
    x = np.arange(-radius, radius + 1)
    k = torch.from_numpy(1.0 / np.sqrt(2.0 * np.pi * t) * np.exp((-x * x) / (2.0 * t))).float()
    k3d = torch.einsum('i,j,k->ijk', k, k, k)
    k3d = k3d / k3d.sum()
    vol_3d = F.conv3d(vol_in, k3d.reshape(1, 1, *k3d.shape), stride=1, padding=len(k) // 2)

    # Separable 1D convolution
    k1d = k.view(1, 1, -1)
    for _ in range(3):
        vol = F.conv1d(vol.reshape(-1, 1, vol.size(2)), k1d, padding=len(k) // 2).view(*vol.shape)
        vol = vol.permute(2, 0, 1)
    print((vol_3d - vol).abs().max())  # something ~1e-7
    print(torch.allclose(vol_3d, vol, atol=1e-6))

def volumetric_gaussian_blur(vol, blur_sigma, separate=False):

    t = blur_sigma**2
    radius = math.ceil(3 * blur_sigma)
    x = np.arange(-radius, radius + 1)
    if vol.dtype == torch.double:
        k = torch.from_numpy(1.0 / np.sqrt(2.0 * np.pi * t) * np.exp((-x * x) / (2.0 * t))).double()
    else:
        k = torch.from_numpy(1.0 / np.sqrt(2.0 * np.pi * t) * np.exp((-x * x) / (2.0 * t))).float()
    # k = torch.from_numpy(cv.getGaussianKernel(ks, blur_sigma)).squeeze().float()

    if vol.device.type == "cuda":
        k = k.to(DCSRN_config.DEVICE)

    if not separate:
        # 1 convolution with a 3D gaussian
        vol_in_3d = vol.reshape(1, 1, *vol.shape)
        k3d = torch.einsum('i,j,k->ijk', k, k, k)
        k3d = k3d / k3d.sum()
        vol_3d = F.conv3d(vol_in_3d, k3d.reshape(1, 1, *k3d.shape), stride=1, padding=len(k) // 2)
        return vol_3d[0,0,:,:,:]
    else:
        # Separable 1D convolutions in 3 directions
        k1d = k.view(1, 1, -1)
        #if vol.device.type == "cuda":
        #    k1d = k1d.to(DCSRN_config.DEVICE)
        for _ in range(3):
            vol = F.conv1d(vol.reshape(-1, 1, vol.size(2)), k1d, padding=len(k) // 2).view(*vol.shape)
            vol = vol.permute(2, 0, 1)
        #print((vol_3d- vol).abs().max()) # something ~1e-7
        #print(torch.allclose(vol_3d, vol, atol=1e-6))
        #vol = vol.reshape(1, *vol.shape)  # If you want to add singleton first dimension
        return vol

def volumetric_gaussian_blur_V2(vol, g, separate=False):

    if not separate:
        # 1 convolution with a 3D gaussian
        #vol_in_3d = vol.unsqueeze(0)
        k3d = torch.einsum('i,j,k->ijk', g, g, g)
        k3d = k3d / k3d.sum()
        vol = F.conv3d(vol, k3d.reshape(1, 1, *k3d.shape), stride=1, padding=len(g) // 2)
        return vol
    else:
        # Separable 1D convolutions in 3 directions
        g1d = g.view(1, 1, -1)  # Maybe this should be g.view(-1, 1, 1)
        #if vol.device.type == "cuda":
        #    k1d = k1d.to(DCSRN_config.DEVICE)
        vol = vol.squeeze()
        for _ in range(3):
            vol = F.conv1d(vol.reshape(-1, 1, vol.size(2)), g1d, padding=len(g) // 2).view(*vol.shape)
            vol = vol.permute(2, 0, 1)
        #print((vol_3d- vol).abs().max()) # something ~1e-7
        #print(torch.allclose(vol_3d, vol, atol=1e-6))
        vol = vol.reshape(1, *vol.shape)  # If you want to add singleton first dimension
        return vol

def compute_S_matrix(vol, sigma, rho):

    t = sigma**2
    r1 = math.ceil(3 * sigma)
    ks = int(2 * r1 + 1)
    k = torch.from_numpy(gauss_dev_1D(t)).float()
    #k = torch.from_numpy(1.0 / np.sqrt(2.0 * np.pi * t) * np.exp((-x * x) / (2.0 * t))).float()

    # Separable 1D convolution in 3 directions
    k1d = k.view(1, 1, -1)
    if vol.device.type == "cuda":
        k1d = k1d.to(DCSRN_config.DEVICE)
    Vx = F.conv1d(vol.reshape(-1, 1, vol.size(2)), k1d, padding=ks // 2).view(*vol.shape)
    Vx.reshape(1, *Vx.shape)

    vol = vol.permute(2, 0, 1)
    Vy = F.conv1d(vol.reshape(-1, 1, vol.size(2)), k1d, padding=ks // 2).view(*vol.shape)
    Vy.reshape(1, *Vy.shape)

    vol = vol.permute(2, 0, 1)
    Vz = F.conv1d(vol.reshape(-1, 1, vol.size(2)), k1d, padding=ks // 2).view(*vol.shape)
    Vz.reshape(1, *Vz.shape)

    r2 = np.ceil(3 * rho)
    ks2 = int(2 * r2 + 1)

    Sxx = test_3d_gaussian_blur(Vx * Vx, ks=ks2, blur_sigma=rho)
    Syy = test_3d_gaussian_blur(Vy * Vy, ks=ks2, blur_sigma=rho)
    Szz = test_3d_gaussian_blur(Vz * Vz, ks=ks2, blur_sigma=rho)
    Sxy = test_3d_gaussian_blur(Vx * Vy, ks=ks2, blur_sigma=rho)
    Sxz = test_3d_gaussian_blur(Vx * Vz, ks=ks2, blur_sigma=rho)
    Syz = test_3d_gaussian_blur(Vy * Vz, ks=ks2, blur_sigma=rho)

    S = torch.stack([Sxx, Syy, Szz, Sxy, Sxz, Syz])

    return S
    #L, V = torch.linalg.eig(S)

    #return L, V, S

def get_structure_tensor3D(vol, sigma, rho, full=False):

    vol_cpu = vol.clone().detach().cpu()

    S = compute_S_matrix(vol_cpu, sigma, rho)
    val, vec = eig_special_3d(S, full)

    return val, vec

def compute_S_matrix_V3(vol, sigma, rho, sep, padding="valid"):

    if vol.dtype == torch.float64:
        sigma = torch.tensor(sigma).double()
        rho = torch.tensor(rho).double()
    elif vol.dtype == torch.float32:
        sigma = torch.tensor(sigma).float()
        rho = torch.tensor(rho).float()

    g, gd = get_gaussian_kernel(sigma, also_dg=True, radius=None)
    k = get_gaussian_kernel(rho, also_dg=False, radius=None)
    #a, ad, add = get_gaussian_kernel(sigma, also_ddg=True, radius=None)

    if vol.device.type == "cuda":
        g = g.to(DCSRN_config.DEVICE)
        gd = gd.to(DCSRN_config.DEVICE)
        k = k.to(DCSRN_config.DEVICE)

    Vx = utils.gdx(vol.unsqueeze(0), g, gd, prepend_one=False, padding=padding)
    Vy = utils.gdy(vol.unsqueeze(0), g, gd, prepend_one=False, padding=padding)
    Vz = utils.gdz(vol.unsqueeze(0), g, gd, prepend_one=False, padding=padding)

    Sxx = volumetric_gaussian_blur_V2(Vx * Vx, k, separate=sep)
    Syy = volumetric_gaussian_blur_V2(Vy * Vy, k, separate=sep)
    Szz = volumetric_gaussian_blur_V2(Vz * Vz, k, separate=sep)
    Sxy = volumetric_gaussian_blur_V2(Vx * Vy, k, separate=sep)
    Sxz = volumetric_gaussian_blur_V2(Vx * Vz, k, separate=sep)
    Syz = volumetric_gaussian_blur_V2(Vy * Vz, k, separate=sep)

    S = torch.cat((Sxx, Syy, Szz, Sxy, Sxz, Syz), dim=0)

    return S.squeeze()



def compute_S_matrix_V2(vol, sigma, rho, sep, padding="valid"):

    prepend_one = False

    Vx = utils.gauss_dev_x(vol, sigma, prepend_one, padding)
    Vy = utils.gauss_dev_y(vol, sigma, prepend_one, padding)
    Vz = utils.gauss_dev_z(vol, sigma, prepend_one, padding)
    d = (max(Vx.shape) - min(Vx.shape))//2
    # Cut to square shapes
    Vx = Vx[d:-d, :, :]
    Vy = Vy[:, d:-d, :]
    Vz = Vz[:, :, d:-d]
    #Vx = Vx[min(Vx.shape), min(Vx.shape), min(Vx.shape)]

    #import nibabel as nib
    #import os
    #final_img = nib.Nifti1Image(Vx.numpy(), np.eye(4))
    #nib.save(final_img, os.path.join("test_images/", 'Vx.nii'))

    #st3d.structure_tensor_3d(vol, sigma, rho, out=None, truncate=3.0)

    Sxx = volumetric_gaussian_blur(Vx * Vx, blur_sigma=rho, separate=sep)
    Syy = volumetric_gaussian_blur(Vy * Vy, blur_sigma=rho, separate=sep)
    Szz = volumetric_gaussian_blur(Vz * Vz, blur_sigma=rho, separate=sep)
    Sxy = volumetric_gaussian_blur(Vx * Vy, blur_sigma=rho, separate=sep)
    Sxz = volumetric_gaussian_blur(Vx * Vz, blur_sigma=rho, separate=sep)
    Syz = volumetric_gaussian_blur(Vy * Vz, blur_sigma=rho, separate=sep)

    # # 3D convolution
    # vol_in_3d = vol.reshape(1, 1, *(Vx * Vx).shape)
    #
    # t = rho**2
    # radius = math.ceil(3 * rho)
    # x = np.arange(-radius, radius + 1)
    # k = torch.from_numpy(1.0 / np.sqrt(2.0 * np.pi * t) * np.exp((-x * x) / (2.0 * t))).float()
    #
    # k3d = torch.einsum('i,j,k->ijk', k, k, k)
    # k3d = k3d / k3d.sum()
    # vol_3d = F.conv3d(vol_in_3d, k3d.reshape(1, 1, *k3d.shape), stride=1, padding=len(k) // 2)

    S = torch.cat((Sxx, Syy, Szz, Sxy, Sxz, Syz), dim=0)

    return S
    #return torch.stack([Sxx, Syy, Szz, Sxy, Sxz, Syz])

def compute_3D_inv_S1xS2_V2(S1, S2):
    D = -S1[0] * S1[1]**2 + S1[0]**2 * S1[1] - S1[4]**2 * S1[1] + 2 * S1[3] * S1[4] * S1[5] - S1[0] * S1[5]**2

    A11 = S1[2]*S1[1]-S1[5]**2 / D
    A12 = S1[4]*S1[5]-S1[2]*S1[3] / D
    A13 = S1[3]*S1[5]-S1[4]*S1[1] / D
    A22 = S1[2]*S1[0]-S1[4]**2 / D
    A23 = S1[3]*S1[4]-S1[0]*S1[5] / D
    A33 = S1[0]*S1[1]-S1[3]**2 / D

    M11 = A11 * S2[0] + A12 * S2[3] + A13 * S2[4]
    M12 = A11 * S2[3] + A12 * S2[1] + A13 * S2[5]
    M13 = A11 * S2[4] + A12 * S2[5] + A13 * S2[2]

    M21 = A12 * S2[0] + A22 * S2[3] + A23 * S2[4]
    M22 = A12 * S2[3] + A22 * S2[1] + A23 * S2[5]
    M23 = A12 * S2[4] + A22 * S2[5] + A23 * S2[2]

    M31 = A13 * S2[0] + A23 * S2[3] + A33 * S2[4]
    M32 = A13 * S2[3] + A23 * S2[1] + A33 * S2[5]
    M33 = A13 * S2[4] + A23 * S2[5] + A33 * S2[2]

    MR1 = torch.cat([M11, M12, M13], dim=1)
    MR2 = torch.cat([M21, M22, M23], dim=1)
    MR3 = torch.cat([M31, M32, M33], dim=1)
    M = torch.cat([MR1, MR2, MR3], dim=2)
    return M

def compute_3D_inv_S1xS2(S1, S2):
    D = -S1[1,1] * S1[1,2] ^ 2 + S1[1,1] ^ 2 * S1[2,2] - S1[1,3] ^ 2 * S1[2,2] + 2 * S1[1,2] * S1[1,3] * S1[2,3] - S1[1,1] * S1[2,3] ^ 2

    A11 = S1[3,3]*S1[2,2]-S1[2,3]**2 / D
    A12 = S1[1,3]*S1[2,3]-S1[3,3]*S1[1,2] / D
    A13 = S1[1,2]*S1[2,3]-S1[1,3]*S1[2,2] / D
    A22 = S1[3,3]*S1[1,1]-S1[1,3]**2 / D
    A23 = S1[1,2]*S1[1,3]-S1[1,1]*S1[2,3] / D
    A33 = S1[1,1]*S1[2,2]-S1[1,2]**2 / D

    M11 = A11 * S2[1, 1] + A12 * S2[1, 2] + A13 * S2[1, 3]
    M12 = A11 * S2[1, 2] + A12 * S2[2, 2] + A13 * S2[2, 3]
    M13 = A11 * S2[1, 3] + A12 * S2[2, 3] + A13 * S2[3, 3]

    M21 = A12 * S2[1, 1] + A22 * S2[1, 2] + A23 * S2[1, 3]
    M22 = A12 * S2[1, 2] + A22 * S2[2, 2] + A23 * S2[2, 3]
    M23 = A12 * S2[1, 3] + A22 * S2[2, 3] + A23 * S2[3, 3]

    M31 = A13 * S2[1, 1] + A23 * S2[1, 2] + A33 * S2[1, 3]
    M32 = A13 * S2[1, 2] + A23 * S2[2, 2] + A33 * S2[2, 3]
    M33 = A13 * S2[1, 3] + A23 * S2[2, 3] + A33 * S2[3, 3]

    M = torch.stack([M11, M12, M13, M21, M22, M23, M31, M32, M33], dim=1)

def compute_3D_eigenvalues(M):

    # p1 = A(1, 2) ^ 2 + A(1, 3) ^ 2 + A(2, 3) ^ 2
    #
    # if (p1 == 0):
    #     # A is diagonal.
    #     eig1 = A(1, 1)
    #     eig2 = A(2, 2)
    #     eig3 = A(3, 3)
    # else:
    #     q = trace(A) / 3 # % trace(A) is the sum of all diagonal values
    #     p2 = (A(1, 1) - q) ^ 2 + (A(2, 2) - q) ^ 2 + (A(3, 3) - q) ^ 2 + 2 * p1
    #     p = sqrt(p2 / 6)
    #     B = (1 / p) * (A - q * I) #% I is the identity matrix
    #     r = det(B) / 2
    #
    # if (r <= -1):
    #     phi = pi / 3
    # elif (r >= 1):
    #     phi = 0
    # else:
    #     phi = acos(r) / 3
    #
    # eig1 = q + 2 * p * cos(phi)
    # eig3 = q + 2 * p * cos(phi + (2 * pi / 3))
    # eig2 = 3 * q - eig1 - eig3 # % since trace(A) = eig1 + eig2 + eig3

    L = torch.linalg.eigvals(M)
    L = torch.clamp(L, min=1)
    return L

def compute_3D_distance(L):
    d = torch.sqrt((torch.log(L) ** 2).sum(dim=0))
    return d
    #vals = np.linalg.eigvals(M)
    #return np.sqrt((np.log(vals)**2).sum())

def sample_patch(size, rescale=False, subject=None):
    sampler = tio.data.LabelSampler(size, label_name="seg")
    patch = next(iter(sampler(subject)))
    # patch_lo_res = DCSRN_config.DOWNSCALE_FUNC(patch)
    vol = patch.lo_res.data[0]
    vol = (vol - patch['mean_lo_res']) / patch['std_lo_res']
    # vol = vol.float()
    if rescale:
        rescale = tio.transforms.RescaleIntensity((-1.0,1.0))
        vol = rescale(vol.reshape(1, *vol.shape))[0]
    print("Min", np.min(vol.numpy()))
    print("Max", np.max(vol.numpy()))
    vol = vol.double()  # Maximum precision ensures that numerical errors are minimized
    return vol

def test_invariance_3D_torch(n):
    dist_r_list = []
    dist_list = []
    dist_r_list_good = []
    dist_list_good = []
    for i in range(n):
        print("Sampling volume", i + 1, "/", n)
        vol1 = sample_patch(10, False, DCSRN_config.femur_21_1)
        #vol1 = vol1.to(DCSRN_config.DEVICE)
        S1_ele = structure_tensor_3d(vol1, 1.0, 1.0)
        S1_ele = torch.tensor(S1_ele)
        #S1_ele = normalize_S(S1_ele)
        S1_full = convert_3x3_st(S1_ele, tensor=True)

        vol2 = sample_patch(10, False, DCSRN_config.femur_21_1)
        #vol2 = vol2.to(DCSRN_config.DEVICE)
        S2_ele = structure_tensor_3d(vol2, 1.0, 1.0)
        S2_ele = torch.tensor(S2_ele)
        #S2_ele = normalize_S(S2_ele)
        S2_full = convert_3x3_st(S2_ele, tensor=True)

        theta = np.random.uniform(low=0, high=90)
        phi = np.random.uniform(low=0, high=90)
        psi = np.random.uniform(low=0, high=90)
        Rxyz = rotation3D(theta_deg=theta, phi_deg=phi, psi_deg=psi, as_tensor=True)

        s = np.random.uniform(1, 10)
        Rxyz = s*Rxyz
        S1_prime_full = matmul_3x3(matmul_3x3(Rxyz.T, S1_full, tensor=True), Rxyz, tensor=True)
        S2_prime_full = matmul_3x3(matmul_3x3(Rxyz.T, S2_full, tensor=True), Rxyz, tensor=True)
        S1_prime_ele = convert_1x6_st(S1_prime_full, tensor=True)
        S2_prime_ele = convert_1x6_st(S2_prime_full, tensor=True)

        d_r_good, d_r_good_points = log_euclidean_metric_torch(S1_prime_ele, S2_prime_ele)
        #d_r_good = log_euclidean_metric(S1_prime_ele, S2_prime_ele)
        # dist_r = np.sum((convert_3x3_st(S1_prime_ele) - convert_3x3_st(S2_prime_ele))**2)
        d_r_bad = torch.sqrt(torch.sum(((S1_prime_ele) - (S2_prime_ele)) ** 2))
        dist_r_list.append(d_r_bad)
        dist_r_list_good.append(d_r_good)

        #d_good = log_euclidean_metric(S1_ele, S2_ele)
        d_good, d_good_points = log_euclidean_metric_torch(S1_ele, S2_ele)
        # dist = np.sum((convert_3x3_st(S1_ele) - convert_3x3_st(S2_ele))**2)
        d_bad = torch.sqrt(torch.sum(((S1_ele) - (S2_ele)) ** 2))
        dist_list.append(d_bad)
        dist_list_good.append(d_good)
        print("dist good, dist rot good", d_good, d_r_good)
        print("dist bad, dist rot bad", d_bad, d_r_bad)

        del S1_prime_ele
        del S2_prime_ele
        del S1_ele
        del S2_ele

    #matplotlib.rcParams['mathtext.fontset'] = 'custom'
    #matplotlib.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
    #matplotlib.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
    #matplotlib.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'
    #plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams["font.family"] = "serif"
    plt.rcParams["mathtext.fontset"] = "dejavuserif"
    matplotlib.rcParams.update({'font.size': 12})
    plt.figure(figsize=(17, 5), constrained_layout=True)
    #plt.subplots_adjust(hspace=1.0)

    plt.subplot(1, 3, 1)
    plt.scatter(dist_list, dist_r_list, marker="o")
    plt.title("Euclidean distance metric")
    plt.xlabel(r'$d_{E}(S_{1}, S_{2})$')
    plt.ylabel(r'$d_{E}(s^{2}R^{\top}S_{1}R, s^{2}R^{\top}S_{2}R)$')

    plt.subplot(1, 3, 2)
    plt.scatter(dist_list_good, dist_r_list_good, marker="o")
    plt.title("Log-Euclidean distance metric")
    plt.xlabel(r'$d_{LE}(S_{1}, S_{2})$')
    plt.ylabel(r'$d_{LE}(s^{2}R^{\top}S_{1}R, s^{2}R^{\top}S_{2}R)$')

    plt.subplot(1, 3, 3)
    #plt.scatter(dist_list, dist_list_good, marker="o")
    plt.scatter(dist_r_list, dist_r_list_good, marker="o")
    plt.xlabel(r'$d_{E}(s^{2}R^{\top}S_{1}R, s^{2}R^{\top}S_{2}R)$')
    plt.ylabel(r'$d_{LE}(s^{2}R^{\top}S_{1}R, s^{2}R^{\top}S_{2}R)$')
    plt.title("Euclidean vs. Log-Euclidean distance metric")
    #plt.legend("Rotated ")

    plt.figure(figsize=(11, 5), constrained_layout=True)
    # plt.subplots_adjust(hspace=1.0)
    plt.subplot(1, 2, 1)
    plt.scatter(dist_list, dist_r_list, marker="o")
    plt.title("Euclidean distance metric")
    plt.xlabel(r'$d_{E}(S_{1}, S_{2})$')
    plt.ylabel(r'$d_{E}(s^{2}R^{\top}S_{1}R, s^{2}R^{\top}S_{2}R)$')

    plt.subplot(1, 2, 2)
    plt.scatter(dist_list_good, dist_r_list_good, marker="o")
    plt.title("Log-Euclidean distance metric")
    plt.xlabel(r'$d_{LE}(S_{1}, S_{2})$')
    plt.ylabel(r'$d_{LE}(s^{2}R^{\top}S_{1}R, s^{2}R^{\top}S_{2}R)$')

    plt.show()

if __name__ == "__main__":

    vol = torch.randn([40, 40, 40]).float()
    sigma = 0.5
    rho = 2.0

    #S = compute_S_matrix(vol, sigma, rho)

    if False:
        import torchio as tio
        sampler = tio.data.LabelSampler(40, label_name="seg")
        patch = next(iter(sampler(DCSRN_config.femur_01_b_1)))
        #patch_lo_res = DCSRN_config.DOWNSCALE_FUNC(patch)
        vol = patch.lo_res.data[0]
        vol = (vol - patch['mean_lo_res']) / patch['std_lo_res']
        vol = vol.float()
    #vol1 = sample_patch(80, rescale=False)
    load_vol1 = False  # True
    if load_vol1:
        import pickle
        with open('vol1.pickle', 'rb') as handle:
            vol1 = pickle.load(handle)
    else:
        vol1 = sample_patch(40, rescale=False, subject=DCSRN_config.femur_15_1)

    vol2 = sample_patch(40, rescale=False, subject=DCSRN_config.femur_21_1)

    #vol1 = torch.randn((160, 160, 160)).double()
    #vol2 = torch.randn((160, 160, 160)).double()
    vol2.requires_grad_()
    #vol1 = vol1.to(DCSRN_config.DEVICE)
    #vol2 = vol2.to(DCSRN_config.DEVICE)
    with torch.cuda.amp.autocast(dtype=torch.float16):

        #with torch.cuda.amp.autocast(dtype=torch.float16):

        with torch.cuda.amp.autocast(dtype=torch.float16, enabled=False):
            S1_ele = compute_S_matrix_V3(vol1, sigma, rho, sep=True, padding='valid')
            S2_ele = compute_S_matrix_V3(vol2, sigma, rho, sep=True, padding='valid')
            #dist = log_euclidean_metric_torch(S1_ele, S2_ele)

        print("S1_ele shape", S1_ele.shape)
        val_true, vec_true = eig_special_3d(S1_ele, full=True)
        with torch.autograd.set_detect_anomaly(True):
            val, vec = eig_special_3d_torch(S1_ele, full=True)

        print("Is torch val allclose to eig_special_3d", torch.allclose(val, torch.tensor(val_true)))
        print("Is torch vec allclose to eig_special_3d", torch.allclose(vec, torch.tensor(vec_true)))
        print("max diff vec vec_true", torch.max(vec - torch.tensor(vec_true)))
    from st3d_vedrana import show_vol_flow, show_vol_orientation, structure_tensor, eig_special, fan_coloring
    #import scipy
    #volume = scipy.io.loadmat("C:/Users/augus/OneDrive/Dokumenter/GitHub/structure-tensor-master-vedrana/structure-tensor-master/example_data_3D/multi_cube.mat")['vol']
    sigma = 0.5
    rho = 2
    S = structure_tensor(vol1.numpy(), sigma, rho)
    val, vec = eig_special(S)
    show_vol_flow(vol1, vec[0:2].reshape((2,) + vol1.shape), s=3, double_arrow=True)
    show_vol_orientation(vol1.numpy(), vec, coloring=fan_coloring)
    plt.show()

    dist, dist_points = log_euclidean_metric_torch(S1_ele=S1_ele, S2_ele=S2_ele)
    print("dist * 10e-8 =", dist*10e-8)

    test_invariance_3D_torch(50)

    #SR1 = torch.cat([S[0], S[3], S[4]], dim=1)
    #SR2 = torch.cat([S[3], S[1], S[5]], dim=1)
    #SR3 = torch.cat([S[4], S[5], S[2]], dim=1)
    #S_full = torch.cat([SR1, SR2, SR3], dim=2)



    #M = compute_3D_inv_S1xS2_V2(S, S)

    #L = compute_3D_eigenvalues(M)

    #val, vec = eig_special_3d(S, full=False)

    print("Done")

