import glob
import os
from tqdm import tqdm
import torchvision
#import torchvision.transforms as transforms
from torch.utils.data import DataLoader
#from dataset import EM_DATA
#from tqdm import tqdm_notebook as tqdm
#from tqdm import tqdm_notebook as tqdm
import torch
import torch.nn as nn
import nibabel as nib
import torch.optim as optim
#import torch.nn.functional as F
import numpy as np
import matplotlib.pyplot as plt
#import copy
#matplotlib inline
#from PIL import Image
#import cv2 as cv
import PIL.Image as Image
import torchio as tio
import torchio.transforms as tiotransforms
import gc
from utils import ICNR

import DCSRN_config
#from dataset_SRGAN import IMAGEDATA
#import SRGAN_config
from dataset_DCSRN import VOXELDATA, CUSTOM_QUEUE
from Loss_functions import VGGLoss, VGGLoss3D, GradientLoss3D, TotalVariationLoss3D, TextureLoss3D, StructureLoss3D, LaplacianLoss3D
#import kornia as korn
#from torch.profiler import profile, record_function, ProfilerActivity
TOTAL_GPU_MEM = torch.cuda.get_device_properties(0).total_memory/10**9

#from WGAN import train_mixed_precision_WGAN_GP_K1, train_mixed_precision_WGAN_GP_V2, performance_metrics
from WGAN import performance_metrics

import pickle

def toggle_grad(model, on_or_off):
    # https://github.com/ajbrock/BigGAN-PyTorch/blob/master/utils.py#L674
    for param in model.parameters():
        param.requires_grad = on_or_off

def compute_discriminator_loss(prop_real, prop_fake, loss_fn_dict):

    # Formulate Discriminator loss: Max log(D(I_HR)) + log(1 - D(G(I_LR)))
    dis_loss_fake = loss_fn_dict["BCE_Logistic_Loss"](prop_fake, torch.zeros_like(prop_fake))
    dis_loss_real = loss_fn_dict["BCE_Logistic_Loss"](prop_real, torch.ones_like(prop_real) - 0.1 * torch.ones_like(prop_real))  # Added one-side label smoothing
    dis_loss = dis_loss_real + dis_loss_fake

    return dis_loss

def compute_generator_loss(prop_fake, real_hi_res, fake_hi_res, loss_fn_dict, epoch, loss_dict, n_samples):

    # Formulate Generator loss: Min log(I_HR - G(I_LR)) <-> Max log(D(G(I_LR)))
    adv_loss = DCSRN_config.LOSS_WEIGHTS["ADV"] * loss_fn_dict["BCE_Logistic_Loss"](prop_fake, torch.ones_like(prop_fake))
    if epoch >= DCSRN_config.DISABLE_LOSSES_FROM_EPOCH:
        gen_loss = 10 * adv_loss
    else:
        gen_loss = torch.tensor(0.0).to(DCSRN_config.DEVICE)
        if DCSRN_config.LOSS_WEIGHTS["L1"] != 0:
            gen_loss_L1 = DCSRN_config.LOSS_WEIGHTS["L1"] * loss_fn_dict["L1_Loss"](real_hi_res, fake_hi_res)
            gen_loss += gen_loss_L1
            loss_dict["L1"][epoch] += gen_loss_L1.item() / n_samples
        if DCSRN_config.LOSS_WEIGHTS["MSE"] != 0:
            gen_loss_MSE = DCSRN_config.LOSS_WEIGHTS["MSE"] * loss_fn_dict["MSE_Loss"](real_hi_res, fake_hi_res)
            gen_loss += gen_loss_MSE
            #loss_dict["MSE"].append(gen_loss_MSE.item())
            loss_dict["MSE"][epoch] += gen_loss_MSE.item() / n_samples
        if DCSRN_config.LOSS_WEIGHTS["GRAD"] != 0:
            # We should implement this as three 1-D convolutions with gaussian derivatives.
            gen_loss_GRAD = DCSRN_config.LOSS_WEIGHTS["GRAD"] * loss_fn_dict["GRAD_Loss"](real_hi_res, fake_hi_res)
            gen_loss += gen_loss_GRAD
            #loss_dict["GRAD"].append(gen_loss_GRAD.item())
            loss_dict["GRAD"][epoch] += gen_loss_GRAD.item() / n_samples
        if DCSRN_config.LOSS_WEIGHTS["LAPLACE"] != 0:
            gen_loss_LAPLACE = DCSRN_config.LOSS_WEIGHTS["LAPLACE"] * loss_fn_dict["LAPLACE_Loss"](real_hi_res, fake_hi_res)
            gen_loss += gen_loss_LAPLACE[0]
            #loss_dict["LAPLACE"].append(gen_loss_LAPLACE[0].item())
            loss_dict["LAPLACE"][epoch] += gen_loss_LAPLACE[0].item() / n_samples
        if DCSRN_config.LOSS_WEIGHTS["VGG3D"] != 0:
            gen_loss_VGG3D = DCSRN_config.LOSS_WEIGHTS["VGG3D"] * loss_fn_dict["VGG3D_Loss"](real_hi_res, fake_hi_res)
            #gen_loss += gen_loss_VGG3D.item()
            gen_loss += gen_loss_VGG3D[0]
            #loss_dict["VGG3D"].append(gen_loss_VGG3D[0].item())
            loss_dict["VGG3D"][epoch] += gen_loss_VGG3D[0].item() / n_samples
        if DCSRN_config.LOSS_WEIGHTS["TV3D"] != 0:
            gen_loss_TV3D = DCSRN_config.LOSS_WEIGHTS["TV3D"] * loss_fn_dict["TV3D_Loss"](fake_hi_res)
            gen_loss += gen_loss_TV3D
            #loss_dict["TV3D"].append(gen_loss_TV3D.item())
            loss_dict["TV3D"][epoch] += gen_loss_TV3D.item() / n_samples
        if DCSRN_config.LOSS_WEIGHTS["STRUCTURE_TENSOR"] != 0:
            #from Loss_functions import ST_func
            #st = ST_func(real_hi_res, fake_hi_res)
            #gen_loss_STRUCTURE_TENSOR = ST_func.apply(real_hi_res, fake_hi_res)
            with torch.cuda.amp.autocast(dtype=torch.float16, enabled=False):
                gen_loss_STRUCTURE_TENSOR = DCSRN_config.LOSS_WEIGHTS["STRUCTURE_TENSOR"] * loss_fn_dict["STRUCTURE_TENSOR_Loss"](real_hi_res, fake_hi_res)
                #print("Log-euclidean distance:", gen_loss_STRUCTURE_TENSOR)
            gen_loss += gen_loss_STRUCTURE_TENSOR[0]
            #loss_dict["STRUCTURE_TENSOR"].append(gen_loss_STRUCTURE_TENSOR[0].item())
            loss_dict["STRUCTURE_TENSOR"][epoch] += gen_loss_STRUCTURE_TENSOR[0].item() / n_samples

        gen_loss += adv_loss

    return gen_loss




def train_mixed_precision(generator, discriminator, opt_gen, opt_dis, loss_fn_dict, epochs, train_loader, test_loader, print_status):
    """
    Mixed precision training functions for mDCSRN-GAN
    :param generator:
    :param discriminator:
    :param opt_gen:
    :param opt_dis:
    :param loss_fn_dict:
    :param epochs:
    :param train_loader:
    :param test_loader:
    :param print_status:
    :return:
    """

    torch.backends.cudnn.benchmark = True
    #gradient_penalty = False  # Experimental
    gradient_clip = True  # Experimental
    # Sigmoid function used for evaluating probability of discriminator
    sigmoid_func = nn.Sigmoid()

    dis_train_losses = []
    gen_train_losses = []
    dis_valid_losses = []  # This list is empty for WGAN-GP
    gen_valid_losses = []  # This list is empty for WGAN-GP
    psnr_valid_list = []
    ssim_valid_list = []
    dis_guesses_real_valid = []
    dis_guesses_fake_valid = []
    gen_train_loss_dict = {"L1": torch.zeros(epochs), "MSE": torch.zeros(epochs), "GRAD": torch.zeros(epochs), "LAPLACE": torch.zeros(epochs), "VGG3D": torch.zeros(epochs), "TV3D": torch.zeros(epochs), "STRUCTURE_TENSOR": torch.zeros(epochs)}
    gen_valid_loss_dict = {"L1": torch.zeros(epochs), "MSE": torch.zeros(epochs), "GRAD": torch.zeros(epochs), "LAPLACE": torch.zeros(epochs), "VGG3D": torch.zeros(epochs), "TV3D": torch.zeros(epochs), "STRUCTURE_TENSOR": torch.zeros(epochs)}

    # Placeholders for validation losses/guesses
    dis_valid_loss, gen_valid_loss = 0, 0
    dis_guess_real_valid, dis_guess_fake_valid = 0, 0
    # Placeholders for validation performance metrics
    psnr_valid, ssim_valid = 0, 0

    # Can only be used with the custom queue
    #from multiprocessing import Process
    #p1 = Process(target=train_loader.dataset.queue._fill())

    # Creates a GradScaler once at the beginning of training.
    #scaler = torch.cuda.amp.GradScaler()
    gen_scaler = torch.cuda.amp.GradScaler()
    dis_scaler = torch.cuda.amp.GradScaler()

    for epoch in range(epochs):
        dis_train_loss, gen_train_loss = 0, 0

        torch.cuda.empty_cache()
        torch.cuda.reset_peak_memory_stats()
        generator.train()  # Set the model to train mode. This will enable dropout and so on if implemented
        discriminator.train()

        #toggle_grad(generator, False)

        #for batch_idx, (real_hi_res, lo_res) in enumerate(train_loader):
        for batch_idx, (real_hi_res, lo_res) in enumerate(tqdm(train_loader, ascii=True, desc=("Epoch %d" % (epoch + 1)))):

            # The next to lines allows the model to run on the gpu if cuda is available
            real_hi_res = real_hi_res.to(DCSRN_config.DEVICE)
            lo_res = lo_res.to(DCSRN_config.DEVICE)
            batch_size = real_hi_res.shape[0]
            #print("Memory allocation after batch load: %0.3f Gb / %0.3f Gb" % (torch.cuda.memory_allocated()/10**9, TOTAL_GPU_MEM))

            with torch.cuda.amp.autocast(dtype=torch.float16):
                # Generate fake high resolution images
                fake_hi_res = generator(lo_res)
                #print("Memory allocation after generator: %0.3f Gb / %0.3f Gb" % (torch.cuda.memory_allocated()/10**9, TOTAL_GPU_MEM))
                # Compute discriminator probabilities on real and fake
                prop_real = discriminator(real_hi_res)
                #print("Memory allocation after disc real: %0.3f Gb / %0.3f Gb" % (torch.cuda.memory_allocated()/10**9, TOTAL_GPU_MEM))
                prop_fake = discriminator(fake_hi_res.detach())
                #print("Memory allocation after disc fake: %0.3f Gb / %0.3f Gb" % (torch.cuda.memory_allocated()/10**9, TOTAL_GPU_MEM))
                dis_loss = compute_discriminator_loss(prop_real, prop_fake, loss_fn_dict)

            # Compute average epoch loss
            dis_train_loss += dis_loss.item() / len(train_loader)

            # Train discriminator
            opt_dis.zero_grad()  # set parameter gradients to zero

            dis_scaler.scale(dis_loss).backward()  # backward-pass to compute gradients
            #print("Memory allocation disc backward: %0.3f Gb / %0.3f Gb" % (torch.cuda.memory_allocated() / 10 ** 9, TOTAL_GPU_MEM))
            if gradient_clip == True:
                # Unscales the gradients of optimizer's assigned params in-place
                dis_scaler.unscale_(opt_dis)
                # Since the gradients of optimizer's assigned params are unscaled, clips as usual:
                print("Discriminator gradient norm:", torch.nn.utils.clip_grad_norm_(discriminator.parameters(), max_norm=100).item())
                #torch.nn.utils.clip_grad_norm_(discriminator.parameters(), max_norm=100)

            dis_scaler.step(opt_dis)  # update weights
            dis_scaler.update()
            #gc.collect()

            with torch.cuda.amp.autocast(dtype=torch.float16):
                prop_fake = discriminator(fake_hi_res)  # Compute prop_fake again, as the value was discarded by backward
                gen_loss = compute_generator_loss(prop_fake, real_hi_res, fake_hi_res, loss_fn_dict, epoch, gen_train_loss_dict, len(train_loader))

            # Compute average epoch loss
            gen_train_loss += gen_loss.item() / len(train_loader)

            # Train generator
            opt_gen.zero_grad()

            gen_scaler.scale(gen_loss).backward()
            if gradient_clip == True:
                # Unscales the gradients of optimizer's assigned params in-place
                gen_scaler.unscale_(opt_gen)
                # Since the gradients of optimizer's assigned params are unscaled, clips as usual:
                print("Generator gradient norm:", torch.nn.utils.clip_grad_norm_(generator.parameters(), max_norm=10).item())
                #torch.nn.utils.clip_grad_norm_(generator.parameters(), max_norm=10)
            #print("Memory allocation gen backward: %0.3f Gb / %0.3f Gb" % (torch.cuda.memory_allocated() / 10 ** 9, TOTAL_GPU_MEM))
            gen_scaler.step(opt_gen)
            gen_scaler.update()
            #gc.collect()

            # Try deletion of variables to save memory
            #torch.cuda.empty_cache()

        max_memory_allocated = torch.cuda.max_memory_allocated()
        max_memory_reserved = torch.cuda.max_memory_reserved()
        print("Maximum memory reserved during training step: %0.3f Gb / %0.3f Gb" % (max_memory_reserved / 10 ** 9, TOTAL_GPU_MEM))


        # Incomment these two lines if you want to fill queue before validation step
        #print("Filling patch queue before validation step")
        #train_loader.dataset.queue._fill()

        #p1.start()
        #next(iter(train_loader))
        # Validation step every N epoch
        # Evaluate, do not propagate gradients
        if epoch % DCSRN_config.VALIDATION_FREQUENCY == 0:
            dis_valid_loss, gen_valid_loss = 0, 0
            dis_guess_real_valid, dis_guess_fake_valid = 0, 0
            psnr_valid, ssim_valid = 0, 0

            with torch.inference_mode():
                generator.eval()
                discriminator.eval()

                # Just load a single batch from the test loader
                #real_hi_res, lo_res = next(iter(test_loader))
                # Or validate using the whole validation set
                for batch_idx, (real_hi_res, lo_res) in enumerate(tqdm(test_loader, ascii=True, desc=("Epoch %d validation" % (epoch + 1)))):
                #for batch_idx, (real_hi_res, lo_res) in enumerate(test_loader):
                    real_hi_res = real_hi_res.to(DCSRN_config.DEVICE)
                    lo_res = lo_res.to(DCSRN_config.DEVICE)

                    with torch.cuda.amp.autocast(dtype=torch.float16):
                        # Generate fake high resolution images
                        fake_hi_res = generator(lo_res)
                        prop_real = discriminator(real_hi_res)
                        prop_fake = discriminator(fake_hi_res.detach())
                        dis_loss = compute_discriminator_loss(prop_real, prop_fake, loss_fn_dict)

                    # Compute average epoch loss
                    dis_valid_loss += dis_loss.item() / len(test_loader)

                    # Compute average discriminator probability on fake and real
                    dis_guess_real_valid += (torch.sum(sigmoid_func(prop_real))).item() / len(test_loader.dataset)
                    dis_guess_fake_valid += (torch.sum(sigmoid_func(prop_fake))).item() / len(test_loader.dataset)

                    psnr, ssim = performance_metrics(real_hi_res, fake_hi_res)
                    psnr_valid += psnr.item() / len(test_loader.dataset)
                    ssim_valid += ssim.item() / len(test_loader.dataset)

                    with torch.cuda.amp.autocast(dtype=torch.float16):
                        prop_fake = discriminator(fake_hi_res)  # Compute prop_fake again, as the value was discarded by backward
                        gen_loss = compute_generator_loss(prop_fake, real_hi_res, fake_hi_res, loss_fn_dict, epoch, gen_valid_loss_dict, len(test_loader))

                    # Compute average epoch loss
                    gen_valid_loss += gen_loss.item() / len(test_loader)

        if print_status:
            if epoch >= DCSRN_config.DISABLE_LOSSES_FROM_EPOCH:
                print("LOSSES DISABLED EXCEPT ADVERSARIAL LOSS")
            print(f"Discriminator training/validation loss in epoch {epoch+1}/{epochs} was {dis_train_loss:.4f}/{dis_valid_loss:.4f}")
            print(f"Generator GAN training/validation loss in epoch {epoch+1}/{epochs} was {gen_train_loss:.4f}/{gen_valid_loss:.4f}")
            print(f"Average PSNR of validation set in epoch {epoch + 1}/{epochs} was {psnr_valid:.4f}")
            print(f"Average SSIM of validation set in epoch {epoch + 1}/{epochs} was {ssim_valid:.4f}")
            print(f"Average discriminator guess on reals in epoch {epoch + 1}/{epochs} was {dis_guess_real_valid:.4f}")
            print(f"Average discriminator guess on fakes in epoch {epoch + 1}/{epochs} was {dis_guess_fake_valid:.4f}")
            dis_train_losses.append(dis_train_loss)
            gen_train_losses.append(gen_train_loss)
            dis_valid_losses.append(dis_valid_loss)
            gen_valid_losses.append(gen_valid_loss)
            psnr_valid_list.append(psnr_valid)
            ssim_valid_list.append(ssim_valid)
            dis_guesses_real_valid.append(dis_guess_real_valid)
            dis_guesses_fake_valid.append(dis_guess_fake_valid)


        # Save possible checkpoint every N epoch
        if (epoch > 0) and ((epoch + 1) % DCSRN_config.CHECKPOINT_FREQUENCY == 0):  # Save network parameters every 50 epoch
            checkpoint_str = ("CP_%d_" % (epoch + 1))
            # SAVE MODEL
            print(("SAVING NETWORK PARAMETER CHECKPOINT AT EPOCH %d" % (epoch + 1)))
            saved_models_path = os.path.join("saved_models/" + DCSRN_config.OUT_DIR_NAME, DCSRN_config.EXPERIMENT_NAME)
            torch.save(generator.state_dict(), os.path.join(saved_models_path, checkpoint_str + DCSRN_config.GENERATOR_NAME))
            torch.save(discriminator.state_dict(), os.path.join(saved_models_path, checkpoint_str + DCSRN_config.DISCRIMINATOR_NAME))

            out_dict = {
                "Discriminator_train_losses": dis_train_losses,
                "Generator_train_losses": gen_train_losses,
                "Discriminator_valid_losses": dis_valid_losses,
                "Generator_valid_losses": gen_valid_losses,
                "PSNR_valid_scores": psnr_valid_list,
                "SSIM_valid_scores": ssim_valid_list,
                "Discriminator_guesses_fake_valid": dis_guesses_fake_valid,
                "Discriminator_guesses_real_valid": dis_guesses_real_valid,
                "Generator_train_loss_dictionary": gen_train_loss_dict,
                "Generator_valid_loss_dictionary": gen_valid_loss_dict
            }
            # SAVE MODEL LOSSES
            print("SAVING MODEL LOSSES")
            saved_loss_dicts_path = os.path.join("saved_loss_dicts/" + DCSRN_config.OUT_DIR_NAME, DCSRN_config.EXPERIMENT_NAME)
            with open(os.path.join(saved_loss_dicts_path, checkpoint_str + DCSRN_config.OUT_DICT_FILENAME), 'wb') as handle:
                pickle.dump(out_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)

    out_dict = {
        "Discriminator_train_losses": dis_train_losses,
        "Generator_train_losses": gen_train_losses,
        "Discriminator_valid_losses": dis_valid_losses,
        "Generator_valid_losses": gen_valid_losses,
        "PSNR_valid_scores": psnr_valid_list,
        "SSIM_valid_scores": ssim_valid_list,
        "Discriminator_guesses_fake_valid": dis_guesses_fake_valid,
        "Discriminator_guesses_real_valid": dis_guesses_real_valid,
        "Generator_train_loss_dictionary": gen_train_loss_dict,
        "Generator_valid_loss_dictionary": gen_valid_loss_dict
    }

    return out_dict
    #return [dis_train_losses, gen_train_losses, dis_valid_losses, gen_valid_losses, dis_guesses_real_valid, dis_guesses_fake_valid]



def train(generator, discriminator, opt_gen, opt_dis, loss_fn_dict, epochs, data_loader, print_status):
    """
    Simple SRGAN training function
    :param generator: Pytorch model that inherits from torch.nn.Module
    :param discriminator: Pytorch model that inherits from torch.nn.Module
    :param opt_gen: torch.optim optimizer
    :param opt_dis: torch.optim optimizer
    :param loss_fn_MSE: MSE loss function
    :param loss_fn_BCE: BCE loss function
    :param loss_fn_VGG: VGG loss function
    :param epochs: Amount of epochs to train
    :param data_loader: A torch.utils.data.DataLoader object
    :param print_status: Flag to print losses
    :return: Average losses of discriminator and generator
    """

    dis_avg_losses = []
    gen_avg_losses = []

    for epoch in range(epochs):
        dis_avg_loss = 0
        gen_avg_loss = 0

        torch.cuda.empty_cache()
        torch.cuda.reset_peak_memory_stats()
        generator.train()  # Set the model to train mode. This will enable dropout and so on if implemented
        discriminator.train()

        for batch_idx, (real_hi_res, lo_res) in enumerate(data_loader):

            # The next to lines allows the model to run on the gpu if cuda is available
            real_hi_res = real_hi_res.to(DCSRN_config.DEVICE)
            lo_res = lo_res.to(DCSRN_config.DEVICE)
            batch_size = real_hi_res.shape[0]
            #print("Memory allocation after batch load: %0.3f Gb / %0.3f Gb" % (torch.cuda.memory_allocated()/10**9, TOTAL_GPU_MEM))

            # Generate fake high resolution images
            fake_hi_res = generator(lo_res)
            #print("Memory allocation after generator: %0.3f Gb / %0.3f Gb" % (torch.cuda.memory_allocated()/10**9, TOTAL_GPU_MEM))
            #torch.cuda.empty_cache()
            # Compute discriminator probabilities on real and fake
            prop_real = discriminator(real_hi_res)
            #print("Memory allocation after disc real: %0.3f Gb / %0.3f Gb" % (torch.cuda.memory_allocated()/10**9, TOTAL_GPU_MEM))
            prop_fake = discriminator(fake_hi_res.detach())
            #print("Memory allocation after disc fake: %0.3f Gb / %0.3f Gb" % (torch.cuda.memory_allocated()/10**9, TOTAL_GPU_MEM))

            #prop_real = discriminator(real_hi_res)
            #with profile(activities=[ProfilerActivity.CPU, ProfilerActivity.CUDA], profile_memory=True) as prof:
            #    with record_function("discriminator_inference_real"):
            #        prop_fake = discriminator(fake_hi_res.detach())
            #print(prof.key_averages().table(sort_by="cuda_memory_usage", row_limit=10))

            # Formulate Discriminator loss: Max log(D(I_HR)) + log(1 - D(G(I_LR)))
            dis_loss_fake = loss_fn_dict["BCE_Logistic_Loss"](prop_fake, torch.zeros_like(prop_fake))
            dis_loss_real = loss_fn_dict["BCE_Logistic_Loss"](prop_real, torch.ones_like(prop_real) - 0.1*torch.ones_like(prop_real)) # Added one-side label smoothing
            dis_loss = dis_loss_real + dis_loss_fake

            # Compute average epoch loss
            dis_avg_loss += dis_loss.item() / len(data_loader)

            # Train discriminator
            opt_dis.zero_grad()  # set parameter gradients to zero
            dis_loss.backward()  # backward-pass to compute gradients
            #print("Memory allocation disc backward: %0.3f Gb / %0.3f Gb" % (torch.cuda.memory_allocated() / 10 ** 9, TOTAL_GPU_MEM))
            opt_dis.step()  # update weights

            # Formulate Generator loss: Min log(I_HR - G(I_LR)) <-> Max log(D(G(I_LR)))
            prop_fake = discriminator(fake_hi_res)  # Compute prop_fake again, as the value was discarded by backward
            gen_loss_MSE = 1.0 * loss_fn_dict["MSE_Loss"](real_hi_res, fake_hi_res)
            gen_loss_GRAD = 0.1 * loss_fn_dict["GRAD_Loss"](real_hi_res, fake_hi_res)
            gen_loss_VGG = 0.006 * loss_fn_dict["VGG3D_Loss"](real_hi_res, fake_hi_res)
            gen_loss_TV3D = 0.1 * loss_fn_dict["TV3D_Loss"](fake_hi_res)
            #gen_loss_TEXTURE3D = 0.1 * loss_fn_dict["TEXTURE3D_Loss"](real_hi_res, fake_hi_res)
            adv_loss = 10**-3 * loss_fn_dict["BCE_Logistic_Loss"](prop_fake, torch.ones_like(prop_fake))
            gen_loss = gen_loss_MSE + gen_loss_TV3D + gen_loss_GRAD + gen_loss_VGG + adv_loss
            #gen_loss = gen_loss_MSE + adv_loss

            # Compute average epoch loss
            gen_avg_loss += gen_loss.item() / len(data_loader)

            # Train generator
            opt_gen.zero_grad()
            gen_loss.backward()
            #print("Memory allocation gen backward: %0.3f Gb / %0.3f Gb" % (torch.cuda.memory_allocated() / 10 ** 9, TOTAL_GPU_MEM))
            opt_gen.step()

            max_memory_allocated = torch.cuda.max_memory_allocated()
            max_memory_reserved = torch.cuda.max_memory_reserved()
            #print("Maximum memory allocation during epoch: %0.3f Gb / %0.3f Gb" % (max_memory_allocated / 10 ** 9, TOTAL_GPU_MEM))
            print("Maximum memory reserved during epoch: %0.3f Gb / %0.3f Gb" % (max_memory_reserved / 10 ** 9, TOTAL_GPU_MEM))

        if print_status:
            print(f"Discriminator Loss in epoch {epoch}/{epochs} was {dis_avg_loss}")
            print(f"Generator Loss in epoch {epoch}/{epochs} was {gen_avg_loss}")
            dis_avg_losses.append(dis_avg_loss)
            gen_avg_losses.append(gen_avg_loss)

    return dis_avg_losses, gen_avg_losses

def bce_loss(y_real, y_pred):
    """
    Simple binary cross entropy loss
    :param y_real: target value (label)
    :param y_pred: predicted value
    :return: loss
    """
    return torch.mean(y_pred - y_real*y_pred + torch.log(1 + torch.exp(-y_pred)))





class Generator(nn.Module):
    def __init__(self, in_c, scale_factor=2, n_sr_vec = [256,256], n_res_vec=[64,64,64,64,64], n_vec=[64,64,3], k_vec=[9,3,9]):
        super().__init__()

        self.conv0 = nn.Conv2d(in_c, n_vec[0], kernel_size=k_vec[0], stride=1, padding=4)
        self.act0 = nn.PReLU(num_parameters=n_vec[0])

        self.res0 = ResidualBlock(n_vec[0], n_res_vec[0], 3)
        self.res1 = ResidualBlock(n_res_vec[0], n_res_vec[1], 3)
        self.res2 = ResidualBlock(n_res_vec[1], n_res_vec[2], 3)
        self.res3 = ResidualBlock(n_res_vec[2], n_res_vec[3], 3)
        self.res4 = ResidualBlock(n_res_vec[3], n_res_vec[4], 3)

        self.conv1 = nn.Conv2d(n_res_vec[4], n_vec[1], kernel_size=k_vec[1], stride=1, padding=1)
        self.norm0 = nn.BatchNorm2d(n_vec[1])

        self.SR0 = SRBlock(n_vec[1], scale_factor=2)
        self.SR1 = SRBlock(n_vec[1], scale_factor=2)

        self.conv2 = nn.Conv2d(n_vec[1], n_vec[2], kernel_size=k_vec[2], stride=1, padding=4)

    def forward(self, input):

        # Initial convolution and PReLU activation
        x = self.act0(self.conv0(input))

        # Residual block network
        z = self.res0(x)
        z = self.res1(z)
        z = self.res2(z)
        z = self.res3(z)
        z = self.res4(z)

        # Convolution with skip connection after residual network
        z = self.norm0(self.conv1(z))
        z = x + z

        # Super-resolution block
        z = self.SR0(z)
        z = self.SR1(z)

        # Final convolution
        out = self.conv2(z)

        return out


class MultiLevelDenseNet(nn.Module):
    def __init__(self, in_c, k_factor=12, k_size=3):
        super().__init__()

        self.conv0 = nn.Conv3d(in_c, 2 * k_factor, k_size, stride=1, padding=1)

        self.dense_block0 = DenseBlock(2 * k_factor, k_factor, k_size)
        self.dense_block1 = DenseBlock(2 * k_factor, k_factor, k_size)
        self.dense_block2 = DenseBlock(2 * k_factor, k_factor, k_size)
        self.dense_block3 = DenseBlock(2 * k_factor, k_factor, k_size)

        self.compress0 = nn.Conv3d(8 * k_factor, 2 * k_factor, kernel_size=1, stride=1, padding=0)
        self.compress1 = nn.Conv3d(14 * k_factor, 2 * k_factor, kernel_size=1, stride=1, padding=0)
        self.compress2 = nn.Conv3d(20 * k_factor, 2 * k_factor, kernel_size=1, stride=1, padding=0)

        if DCSRN_config.UPSAMPLE_METHOD is None:
            # Direct Feature Combination from paper: https://arxiv.org/pdf/2003.01217v1.pdf
            self.recon = nn.Conv3d(26 * k_factor, 1, kernel_size=1, stride=1, padding=0)
        else:
            # Reconstruction via bottleneck from paper: https://openaccess.thecvf.com/content_ICCV_2017/papers/Tong_Image_Super-Resolution_Using_ICCV_2017_paper.pdf
            self.bottleneck = nn.Conv3d(26 * k_factor, 8 * k_factor, kernel_size=1, stride=1, padding=0)
            # Batch norm operation here?
            if DCSRN_config.UP_FACTOR >= 2:
                self.SR0 = SRBlock3D(8 * k_factor, 8 * k_factor, 6, 2)  # In paper on densenets, they use 8*k channels for in and output
            if DCSRN_config.UP_FACTOR >= 4:
                self.SR1 = SRBlock3D(8 * k_factor, 8 * k_factor, 6, 2)
            # Final reconstruction with kernel size 3
            self.recon = nn.Conv3d(8 * k_factor, 1, kernel_size=k_size, stride=1, padding=1)


    def forward(self, input):

        # Initial convolution with 2k output filters
        x = self.conv0(input)

        z = self.dense_block0(x)
        skip = torch.cat([x, z], 1)
        comp = self.compress0(skip)

        z = self.dense_block1(comp)
        skip = torch.cat([skip, z], 1)
        comp = self.compress1(skip)

        z = self.dense_block2(comp)
        skip = torch.cat([skip, z], 1)
        comp = self.compress2(skip)

        z = self.dense_block3(comp)
        final_skip = torch.cat([skip, z], 1)

        #if DCSRN_config.UPSAMPLE_METHOD is None:
        #    out = self.recon(final_skip)
        #    return out

        z = self.bottleneck(final_skip)
        if DCSRN_config.UP_FACTOR >= 2:
            z = self.SR0(z)
        if DCSRN_config.UP_FACTOR >= 4:
            z = self.SR1(z)
        out = self.recon(z)

        return out


class DenseBlock(nn.Module):
    def __init__(self, in_c, k_factor=12, k_size=3):
        super().__init__()

        # Four or seven dense blocks
        self.dense_unit0 = DenseUnit(in_c, k_factor, k_size)
        self.dense_unit1 = DenseUnit(in_c + k_factor, k_factor, k_size)
        self.dense_unit2 = DenseUnit(in_c + 2*k_factor, k_factor, k_size)
        self.dense_unit3 = DenseUnit(in_c + 3*k_factor, k_factor, k_size)

    def forward(self, input):

        x0 = self.dense_unit0(input)
        skip0 = torch.cat([input, x0], 1)

        x1 = self.dense_unit1(skip0)
        skip1 = torch.cat([skip0, x1], 1)

        x2 = self.dense_unit2(skip1)
        skip2 = torch.cat([skip1, x2], 1)

        x3 = self.dense_unit3(skip2)
        out = torch.cat([skip2, x3], 1)

        return out


class DenseUnit(nn.Module):
    def __init__(self, in_c, k_factor=12, k_size=3):
        super().__init__()

        self.norm0 = nn.BatchNorm3d(num_features=in_c)
        self.act0 = nn.ELU(alpha=1.0)
        self.conv0 = nn.Conv3d(in_c, k_factor, kernel_size=k_size, stride=1, padding=1)

    def forward(self, x):
        return self.conv0(self.act0(self.norm0(x)))


class ResidualBlock(nn.Module):
    def __init__(self, in_c, n=64, k_size=3):
        super().__init__()

        self.conv0 = nn.Conv2d(in_c, n, kernel_size=k_size, stride=1, padding=1)
        self.norm0 = nn.BatchNorm2d(n)
        self.act0 = nn.PReLU(num_parameters=n)

        self.conv1 = nn.Conv2d(n, n, kernel_size=k_size, stride=1, padding=1)
        self.norm1 = nn.BatchNorm2d(n)

    def forward(self, x):
        r = self.norm0(self.conv0(x))
        r = self.act0(r)
        r = self.norm1(self.conv1(r))
        out = x + r

        return out


class SRBlock(nn.Module):
    def __init__(self, in_c, k_size=3, scale_factor=2):
        super().__init__()

        self.conv0 = nn.Conv2d(in_c, in_c * scale_factor**2, kernel_size=k_size, stride=1, padding=1)
        self.shuffle0 = nn.PixelShuffle(scale_factor)
        self.act0 = nn.PReLU(num_parameters=in_c)

    def forward(self, x):
        x = self.conv0(x)
        out = self.act0(self.shuffle0(x))

        return out


class SRBlock3D(nn.Module):
    def __init__(self, in_c, n, k_size=6, pad=2):
        super().__init__()

        if DCSRN_config.UPSAMPLE_METHOD == 'DECONV_NN_RESIZE':
            #self.deconv0 = nn.ConvTranspose3d(in_c, n, kernel_size=k_size, stride=2, padding=pad, bias=0)
            self.deconv0 = nn.ConvTranspose3d(in_c, n, kernel_size=k_size, stride=2, padding=pad)
            self.act0 = nn.PReLU(num_parameters=n)

            # ICNR is an initialization method for sub-pixel convolution which removes checkerboarding
            # From the paper: Checkerboard artifact free sub-pixel convolution.
            # https://gist.github.com/A03ki/2305398458cb8e2155e8e81333f0a965
            weight = ICNR(self.deconv0.weight, initializer=nn.init.normal_, upscale_factor=2, mean=0.0, std=0.02)
            self.deconv0.weight.data.copy_(weight)

    def forward(self, x):

        if DCSRN_config.UPSAMPLE_METHOD == 'DECONV_NN_RESIZE':
            # Taken from this paper, rightmost method in figure 3: https://arxiv.org/pdf/1812.11440.pdf
            x = self.deconv0(x)
            out = self.act0(x)

        return out


class LRconvBlock3D(nn.Module):
    def __init__(self, input_size, in_c, n, k_size, stride):
        super().__init__()

        self.conv0 = nn.Conv3d(in_c, n, kernel_size=k_size, stride=stride, padding=1)
        #self.norm0 = nn.LayerNorm([DCSRN_config.BATCH_SIZE, n, 16, 16, 16])
        self.norm0 = nn.LayerNorm(input_size)
        self.act0 = nn.LeakyReLU(0.2, inplace=True)

    def forward(self, x):
        x = self.conv0(x)
        out = self.act0(self.norm0(x))

        return out


class Discriminator(nn.Module):
    def __init__(self, input_size, in_c, n_conv_vec=[64,64,128,128,256,256,512,512], n_dense=[1024, 1], k_size=3):
        super().__init__()

        self.conv0 = nn.Conv3d(in_c, n_conv_vec[0], kernel_size=k_size, padding=1, stride=1)
        self.act0 = nn.LeakyReLU(0.2, inplace=True)

        dim = [int(np.ceil(input_size/2**i)) for i in range(1,5)]

        self.LRconv0 = LRconvBlock3D(dim[0], n_conv_vec[0], n_conv_vec[1], k_size, stride=2)
        self.LRconv1 = LRconvBlock3D(dim[0], n_conv_vec[1], n_conv_vec[2], k_size, stride=1)
        self.LRconv2 = LRconvBlock3D(dim[1], n_conv_vec[2], n_conv_vec[3], k_size, stride=2)
        self.LRconv3 = LRconvBlock3D(dim[1], n_conv_vec[3], n_conv_vec[4], k_size, stride=1)
        self.LRconv4 = LRconvBlock3D(dim[2], n_conv_vec[4], n_conv_vec[5], k_size, stride=2)
        self.LRconv5 = LRconvBlock3D(dim[2], n_conv_vec[5], n_conv_vec[6], k_size, stride=1)
        self.LRconv6 = LRconvBlock3D(dim[3], n_conv_vec[6], n_conv_vec[7], k_size, stride=2)

        self.flatten = nn.Flatten()
        ll_size = int(n_conv_vec[7] * dim[3]**3)
        self.dense0 = nn.Linear(ll_size, n_dense[0])
        self.act1 = nn.LeakyReLU(0.2, inplace=True)  # Should perhaps be nn.LeakyReLU(0.2, inplace=True)
        self.dense1 = nn.Linear(n_dense[0], n_dense[1])

        self.act_sigmoid = nn.Sigmoid()


    def forward(self, input):

        # Initial convolution and LeakyRelu activation
        x = self.act0(self.conv0(input))

        # LeakyRelu convolution block network
        x = self.LRconv0(x)
        x = self.LRconv1(x)
        x = self.LRconv2(x)
        x = self.LRconv3(x)
        x = self.LRconv4(x)
        x = self.LRconv5(x)
        x = self.LRconv6(x)

        # Dense block network + LeakyRelu
        x = self.dense0(self.flatten(x))
        x = self.act1(x)
        out = self.dense1(x)

        # Final sigmoid activation (Remember to remove if BCEWithLogitsLoss() is used in training loop)
        #out = self.act_sigmoid(out)

        return out


def rescale255(images):

    #i_min = np.min(images)
    #i_max = np.max(images)
    if False:
        for i in range(3):
            i_min = np.min(images[:,:,i])
            i_max = np.max(images[:,:,i])
            images[:,:,i] = (images[:,:,i] - i_min) / (i_max - i_min)

    #images[images > 1] = 1

    images = images / np.max(images)  # Ensure output is between 0 and 1
    images[images < 0] = 0
    images = (images * 255).astype(np.uint8)  # Convert to uint8 in range [0; 255]

    return images

def unnormalize_image(img):
    """
    If the input images are normalized, which is generally recommended, they should be unnormalized before visualization
    This function unnormalizes an image by assuming zero mean and unit standard deviation.
    :param img: Input image (normalized)
    :return: unnormalized output image
    """
    img = img / 2 + 0.5 # unnormalize
    npimg = img.numpy()
    if len(img.size()) > 2:
        return np.transpose(npimg, (1, 2, 0))
    else:
        return npimg

def save_row(images, unnormalize, i, path, file_suffix):
    row = torch.stack(images)
    grid = torchvision.utils.make_grid(row, nrow=len(images), padding=0)
    # grid = grid.numpy()
    # plt.figure(figsize=(15, 15))
    if unnormalize:

        grid = unnormalize_image(grid).squeeze()
        grid = rescale255(grid)
        ###grid = unnormalize_image(grid).squeeze()  # Use this line instead if images have been normalized
    else:
        grid = grid.permute(1, 2, 0).numpy().squeeze()

    ###grid = rescale255(grid)

    # plt.imshow(x_grid.permute(1, 2, 0))
    # plt.title("Sample HR Images", size=20)

    if i < 10:
        string = (path + "slice_0%d_comp" + file_suffix + ".png") % (i)
    else:
        string = (path + "slice_%d_comp" + file_suffix + ".png") % (i)

    im = Image.fromarray(grid)
    im.save(string)

def save_comparisons3D(loader, generator, path, slices, unnormalize, extra_suffix=""):

    hi_res, lo_res = next(iter(loader))

    with torch.inference_mode():
        with torch.cuda.amp.autocast(dtype=torch.float16):
            generator.eval()
            outputs = generator(lo_res.to(DCSRN_config.DEVICE))

    outputs = outputs.float()  # Cast back to float
    #resize = transforms.Resize(size=(DCSRN_config.PATCH_SIZE_HR, DCSRN_config.PATCH_SIZE_HR),
    #         interpolation=transforms.InterpolationMode.BICUBIC)  # Resize to high_res, could also use BILINEAR as this is used in downsampling

    resize3D_bspline = tiotransforms.Resize(
        target_shape=(DCSRN_config.PATCH_SIZE * DCSRN_config.UP_FACTOR,
                      DCSRN_config.PATCH_SIZE * DCSRN_config.UP_FACTOR,
                      DCSRN_config.PATCH_SIZE * DCSRN_config.UP_FACTOR),
                      image_interpolation='BSPLINE')

    resize3D_nearest = tiotransforms.Resize(
        target_shape=(DCSRN_config.PATCH_SIZE * DCSRN_config.UP_FACTOR,
                      DCSRN_config.PATCH_SIZE * DCSRN_config.UP_FACTOR,
                      DCSRN_config.PATCH_SIZE * DCSRN_config.UP_FACTOR),
                      image_interpolation='NEAREST')

    suffixes = ["_X"+extra_suffix, "_Y"+extra_suffix, "_Z"+extra_suffix]
    slice = DCSRN_config.PATCH_SIZE * (DCSRN_config.UP_FACTOR // 2)

    for i in range(loader.batch_size):
        lo_up_all_bspline = resize3D_bspline(lo_res[i, :, :, :, :])
        lo_up_all_nearest = resize3D_nearest(lo_res[i, :, :, :, :])
        hi_res_all = hi_res[i, :, :, :, :]
        out_all = outputs[i, :, :, :, :].cpu().detach()

        for j in range(3):

            # Define slices
            hi = hi_res_all[:, :, :, slice]
            lo_up_bspline = lo_up_all_bspline[:, :, :, slice]
            lo_up_nearest = lo_up_all_nearest[:, :, :, slice]
            out = out_all[:, :, :, slice].cpu().detach()  # Transfer from GPU to CPU

            # Save comparison image
            images = [lo_up_nearest, lo_up_bspline, out, hi]
            save_row(images, unnormalize=unnormalize, i=i, path=path, file_suffix=suffixes[j])

            # Rotate patch
            hi_res_all = hi_res_all.permute(0, 3, 1, 2)
            lo_up_all_bspline = lo_up_all_bspline.permute(0, 3, 1, 2)
            lo_up_all_nearest = lo_up_all_nearest.permute(0, 3, 1, 2)
            out_all = out_all.permute(0, 3, 1, 2)


def save_images_from_list(images, image_paths, image_names):
    for i, image in enumerate(images):
        image = unnormalize_image(image).squeeze()
        image = rescale255(image)
        image = Image.fromarray(image)
        if image_paths is list:
            image.save(image_paths[i] + image_names[i])
        else:
            image.save(image_paths + image_names[i])

def create_output_folders():
    # create output folders
    if os.path.exists(os.path.join("saved_models/" + DCSRN_config.OUT_DIR_NAME, DCSRN_config.EXPERIMENT_NAME)):
        print("Error: Directory", DCSRN_config.EXPERIMENT_NAME, "already exits. Please choose another experiment ID.")
        print("Stopping execution...")
        raise SystemExit(0)

    os.mkdir(os.path.join("saved_models/" + DCSRN_config.OUT_DIR_NAME, DCSRN_config.EXPERIMENT_NAME))
    os.mkdir(os.path.join("saved_loss_dicts/" + DCSRN_config.OUT_DIR_NAME, DCSRN_config.EXPERIMENT_NAME))
    new_model_outputs_path = os.path.join("model_outputs/" + DCSRN_config.OUT_DIR_NAME, DCSRN_config.EXPERIMENT_NAME)
    os.mkdir(new_model_outputs_path)
    os.mkdir(os.path.join(new_model_outputs_path, "test"))
    os.mkdir(os.path.join(new_model_outputs_path, "train"))
    os.mkdir(os.path.join(new_model_outputs_path, "loss_plots"))

    # Example usage
    create_parameter_file("Epochs: %d" % DCSRN_config.EPOCHS,
                          "Patch size HR: %d" % DCSRN_config.PATCH_SIZE_HR,
                          "Patch size LR: %d" % DCSRN_config.PATCH_SIZE,
                          "Upscaling factor: %d" % DCSRN_config.UP_FACTOR,
                          "Batch size: %d" % DCSRN_config.BATCH_SIZE,
                          "Learning rate: %0.4f" % DCSRN_config.LEARNING_RATE,
                          "K factor: %d" % DCSRN_config.K_FACTOR,
                          DCSRN_config.LOSS_WEIGHTS)

def create_parameter_file(*args):
    with open(os.path.join("saved_models/" + DCSRN_config.OUT_DIR_NAME, DCSRN_config.EXPERIMENT_NAME + "/parameters.txt"), "w") as file:
        for param in args:
            file.write(str(param) + "\n")

if __name__ == '__main__':

    print("EXPERIMENT NAME", DCSRN_config.EXPERIMENT_NAME)

    n_conv_vec = [64, 64, 128, 128, 256, 256, 512, 512]
    n_dense = [1024, 1]  # n_dense = [512, 1]
    k_size = 3

    # Create an instance of the Discriminator model
    discriminator = Discriminator(DCSRN_config.PATCH_SIZE_HR, DCSRN_config.IN_C, n_conv_vec, n_dense, k_size).to(DCSRN_config.DEVICE)

    # Create an instance of the Generator model
    generator = MultiLevelDenseNet(DCSRN_config.IN_C, DCSRN_config.K_FACTOR, DCSRN_config.K_SIZE).to(DCSRN_config.DEVICE)

    if DCSRN_config.LOAD_PREVIOUS_MODELS:
        load_path = os.path.join("saved_models/" + DCSRN_config.OUT_DIR_NAME, DCSRN_config.EXPERIMENT_NAME_LOAD)
        if DCSRN_config.LOAD_LATEST_CHECKPOINT:
            # model = UNet().to(SRGAN_config.DEVICE)
            print("LOADING NETWORK PARAMETERS")
            fn_gen = sorted(glob.glob(os.path.join(load_path, "CP_*_" + DCSRN_config.GENERATOR_NAME)), key=len)[-1]
            fn_dis = sorted(glob.glob(os.path.join(load_path, "CP_*_" + DCSRN_config.DISCRIMINATOR_NAME)), key=len)[-1]
            generator.load_state_dict(torch.load(fn_gen))
            discriminator.load_state_dict(torch.load(fn_dis))
        else:
            # model = UNet().to(SRGAN_config.DEVICE)
            print("LOADING NETWORK PARAMETERS")
            generator.load_state_dict(torch.load(os.path.join(load_path, DCSRN_config.GENERATOR_NAME)))
            discriminator.load_state_dict(torch.load(os.path.join(load_path, DCSRN_config.DISCRIMINATOR_NAME)))

    # Test generator on zero data
    x = torch.zeros(DCSRN_config.BATCH_SIZE, 1, DCSRN_config.PATCH_SIZE, DCSRN_config.PATCH_SIZE, DCSRN_config.PATCH_SIZE)
    generator_output = generator(x.to(DCSRN_config.DEVICE))
    ## Test discriminator
    discriminator_output = discriminator(generator_output)

    # See the generator output shape
    print("Shapes")
    print("Input patch shape", x.shape)
    print("Generator output shape", generator_output.shape)
    print("Discriminator output shape", discriminator_output.shape)

    if True:
        test_load = nib.load('3D_datasets/datasets/IXI/train/IXI002-Guys-0828-T1.nii').get_fdata()
        print("IXI sample size", test_load.shape)
        I_HR = test_load[:, :, test_load.shape[-1]//2]
        plt.figure(figsize=(20,10))
        plt.subplot(1, 2, 1)
        plt.imshow(I_HR, cmap='gray')
        plt.subplot(1, 2, 2)
        if DCSRN_config.BATCH_SIZE > 1:
            I_SR = np.squeeze(generator_output.detach().cpu().numpy())[0, :, :, generator_output.shape[-1] // 2]
        else:
            I_SR = np.squeeze(generator_output.detach().cpu().numpy())[:, :, generator_output.shape[-1] // 2]
        plt.imshow(I_SR, cmap='gray')

        plt.show()


    # Load the dataset
    # The width and height specified here are the actual dimensions of the images.
    train_dataset = VOXELDATA(train=True,
                              width=DCSRN_config.PATCH_SIZE_HR,
                              height=DCSRN_config.PATCH_SIZE_HR,
                              depth=DCSRN_config.PATCH_SIZE_HR,
                              _transform_lr=DCSRN_config.TRANSFORM_LR,
                              _transform_hr=DCSRN_config.TRANSFORM_HR,
                              transforms_list=DCSRN_config.T_LIST,
                              data_path=DCSRN_config.DATASET_PATH,
                              train_path=DCSRN_config.TRAIN_PATH,
                              test_path=DCSRN_config.TEST_PATH)

    test_dataset = VOXELDATA(train=False,
                             width=DCSRN_config.PATCH_SIZE_HR,
                             height=DCSRN_config.PATCH_SIZE_HR,
                             depth=DCSRN_config.PATCH_SIZE_HR,
                             _transform_lr=DCSRN_config.TRANSFORM_LR,
                             _transform_hr=DCSRN_config.TRANSFORM_HR,
                             transforms_list=DCSRN_config.T_LIST,
                             data_path=DCSRN_config.DATASET_PATH,
                             train_path=DCSRN_config.TRAIN_PATH,
                             test_path=DCSRN_config.TEST_PATH)

    if DCSRN_config.use_dataset == "2022_QIM_52_Bone":
        if DCSRN_config.PRELOAD_BONE_DATA or DCSRN_config.PRELOAD_BONE_DATA_TEST:
            queue_length_train = 20  # 100  # iterations per epoch = samples_per_volume * n_samples / Batch_size, try 600/100
            samples_per_volume_train = 10
            queue_length_test = 20  # 100
            samples_per_volume_test = 10

            train_dataset = CUSTOM_QUEUE(DCSRN_config.SUBJECTS_DATASET, queue_length_train, samples_per_volume_train, DCSRN_config.SAMPLER, 1, False, True, None)
            test_dataset = CUSTOM_QUEUE(DCSRN_config.SUBJECTS_DATASET_TEST, queue_length_test, samples_per_volume_test, DCSRN_config.SAMPLER, 1, False, False, None)
            #train_dataset.queue._fill()


    # Define pytorch data loaders
    train_loader = DataLoader(train_dataset, DCSRN_config.BATCH_SIZE, shuffle=False, num_workers=0, pin_memory=True)
    test_loader = DataLoader(test_dataset, DCSRN_config.BATCH_SIZE, shuffle=True, num_workers=0, pin_memory=True)
    #test_loader = DataLoader(test_dataset, DCSRN_config.BATCH_SIZE, shuffle=True)
    #print("test")

    #if DCSRN_config.PRELOAD_BONE_DATA or DCSRN_config.PRELOAD_BONE_DATA_TEST:
    #    import time
    #    print("Sleeping for 1 sec")
    #    time.sleep(1)
    #    print("Sleep ended")

    #HR_train, LR_train = next(iter(train_loader))
    #HR_test, LR_test = next(iter(test_loader))

    # See the shapes
    #print("Batch shapes")
    #print(HR_train.shape, LR_train.shape)
    #print(HR_test.shape, LR_test.shape)

    #### TESTING TRAINING FUNCTION ####

    # Now define the optimizer
    # If training using vanilla GAN
    opt_gen = optim.Adam(generator.parameters(), lr=DCSRN_config.LEARNING_RATE, betas=(0.9, 0.999))
    opt_dis = optim.Adam(discriminator.parameters(), lr=DCSRN_config.LEARNING_RATE, betas=(0.9, 0.999))
    # If training using WGAN-GP
    #opt_gen = optim.Adam(generator.parameters(), lr=DCSRN_config.ALPHA_WGAN, betas=(DCSRN_config.BETA1_WGAN, DCSRN_config.BETA2_WGAN))
    #opt_dis = optim.Adam(discriminator.parameters(), lr=DCSRN_config.ALPHA_WGAN, betas=(DCSRN_config.BETA1_WGAN, DCSRN_config.BETA2_WGAN))

    # Loss functions
    loss_fn_mse = nn.MSELoss()
    loss_fn_l1 = nn.L1Loss()
    loss_fn_logistic_bce = nn.BCEWithLogitsLoss()
    loss_fn_bce = nn.BCELoss()
    loss_fn_vgg = VGGLoss(layer_idx=36)
    loss_fn_vgg3D = VGGLoss3D(layer_idx=35)  # Use feature map before activation instead of after
    loss_fn_grad = GradientLoss3D(kernel='diff', order=1, loss_func=nn.L1Loss(), sigma=None) # sigma = 0.8
    loss_fn_laplace = LaplacianLoss3D(sigma=1.0, padding='valid', loss_func=nn.L1Loss())
    loss_fn_tv3D = TotalVariationLoss3D(mode="L2")  # or mode = "sum_of_squares", "L2"
    loss_fn_texture3D = TextureLoss3D(layer_idx=35)
    loss_fn_structure_tensor = StructureLoss3D(sigma=0.5, rho=0.5)

    loss_fn_dict = {
        "MSE_Loss": loss_fn_mse,
        "L1_Loss": loss_fn_l1,
        "BCE_Logistic_Loss": loss_fn_logistic_bce,
        "BCE_Loss": loss_fn_bce,
        "VGG_Loss": loss_fn_vgg,
        "VGG3D_Loss": loss_fn_vgg3D,
        "GRAD_Loss": loss_fn_grad,
        "LAPLACE_Loss": loss_fn_laplace,
        "TV3D_Loss": loss_fn_tv3D,
        "TEXTURE3D_Loss": loss_fn_texture3D,
        "STRUCTURE_TENSOR_Loss": loss_fn_structure_tensor
        }

    # And finally lets train the model. The parameter epochs specifies the number of training iterations.
    # Note that increasing the number of epochs in not always a good idea, especially when there is little training
    # data, as this can lead to overfitting and bad generalization performance on new data.
    # If do_train is False, the script attempts to load a previously training network located in /saved_models
    if DCSRN_config.DO_TRAIN:
        # Create all directories for storing models, losses and outputs
        create_output_folders()

        epoch_list = np.arange(DCSRN_config.EPOCHS)

        # Train mDCSRN-GAN
        out_dict = train_mixed_precision(generator,
                                         discriminator,
                                         opt_gen,
                                         opt_dis,
                                         loss_fn_dict,
                                         DCSRN_config.EPOCHS,
                                         train_loader,
                                         test_loader,
                                         print_status=True)

        # SAVE MODEL
        print("SAVING NETWORK PARAMETERS")
        saved_models_path = os.path.join("saved_models/" + DCSRN_config.OUT_DIR_NAME, DCSRN_config.EXPERIMENT_NAME)
        torch.save(generator.state_dict(), os.path.join(saved_models_path, DCSRN_config.GENERATOR_NAME))
        torch.save(discriminator.state_dict(), os.path.join(saved_models_path, DCSRN_config.DISCRIMINATOR_NAME))

        # SAVE MODEL LOSSES
        print("SAVING MODEL LOSSES")
        saved_loss_dicts = os.path.join("saved_loss_dicts/" + DCSRN_config.OUT_DIR_NAME, DCSRN_config.EXPERIMENT_NAME)
        with open(os.path.join(saved_loss_dicts, DCSRN_config.OUT_DICT_FILENAME), 'wb') as handle:
            pickle.dump(out_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)

        # Make loss plots
        dis_train_loss = out_dict["Discriminator_train_losses"]
        gen_train_loss = out_dict["Generator_train_losses"]
        dis_valid_loss = out_dict["Discriminator_valid_losses"]
        gen_valid_loss = out_dict["Generator_valid_losses"]
        psnr_valid_list = out_dict["PSNR_valid_scores"]
        ssim_valid_list = out_dict["SSIM_valid_scores"]
        dis_guesses_real_valid = out_dict["Discriminator_guesses_real_valid"]
        dis_guesses_fake_valid = out_dict["Discriminator_guesses_fake_valid"]

        if len(dis_valid_loss) == 0:

            # Plot training loss
            fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(10, 10))
            fig.subplots_adjust(hspace=0.5)

            ax1.plot(epoch_list + 1, dis_train_loss)
            ax1.grid(True)
            ax1.set_title("Discriminator loss")
            ax1.set_xlabel('Epoch')
            ax1.set_ylabel('Loss')

            ax2.plot(epoch_list + 1, gen_train_loss)
            ax2.grid(True)
            ax2.set_title("Generator loss")
            ax2.set_xlabel('Epoch')
            ax2.set_ylabel('Loss')

            plt.show()
            #ax1.legend(["Training", "Validation"], loc="upper right")
            # ax1.set_yscale('log')

        else:

            # Plot training loss
            fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(10, 10))
            fig.subplots_adjust(hspace=0.5)

            ax1.plot(epoch_list + 1, dis_train_loss)
            ax1.plot(epoch_list + 1, dis_valid_loss)
            ax1.grid(True)
            ax1.set_title("Discriminator loss")
            ax1.set_xlabel('Epoch')
            ax1.set_ylabel('Loss')
            ax1.legend(["Training", "Validation"], loc="upper right")
            # ax1.set_yscale('log')

            ax2.plot(epoch_list + 1, gen_train_loss)
            ax2.plot(epoch_list + 1, gen_valid_loss)
            ax2.grid(True)
            ax2.set_title("Generator loss")
            ax2.set_xlabel('Epoch')
            ax2.set_ylabel('Loss')
            ax2.legend(["Training", "Validation"], loc="upper right")
            # ax2.set_yscale('log')

            loss_figure_path = os.path.join("model_outputs/" + DCSRN_config.OUT_DIR_NAME,
                                            DCSRN_config.EXPERIMENT_NAME + "/loss_plots/loss_vs_epochs.png")

            #loss_figure_path = "model_outputs/" + DCSRN_config.OUT_DIR_NAME + "/loss_plots/loss_vs_epochs.png"
            fig.savefig(loss_figure_path)

            fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(10, 10))
            fig.subplots_adjust(hspace=0.5)
            for key, value in out_dict["Generator_train_loss_dictionary"].items():
                if torch.sum(value) == 0:
                    continue
                ax1.plot(epoch_list + 1, value, label=key)
            ax1.legend(loc='upper right')
            ax1.grid(True)
            ax1.set_title("Average auxiliary loss values: Training set")
            ax1.set_xlabel('Epoch')
            ax1.set_ylabel('Loss')

            for key, value in out_dict["Generator_valid_loss_dictionary"].items():
                if torch.sum(value) == 0:
                    continue
                ax2.plot(epoch_list + 1, value, label=key)
            ax2.legend(loc='upper right')
            ax2.grid(True)
            ax2.set_title("Average auxiliary loss values: Validation set")
            ax2.set_xlabel('Epoch')
            ax2.set_ylabel('Loss')
            plt.show()

            loss_figure_path = os.path.join("model_outputs/" + DCSRN_config.OUT_DIR_NAME,
                                            DCSRN_config.EXPERIMENT_NAME + "/loss_plots/aux_losses_vs_epochs.png")

            # loss_figure_path = "model_outputs/" + DCSRN_config.OUT_DIR_NAME + "/loss_plots/loss_vs_epochs.png"
            fig.savefig(loss_figure_path)


            fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(10, 10), sharey=True)
            fig.subplots_adjust(hspace=0.5)

            ax1.plot(epoch_list + 1, dis_guesses_real_valid)
            ax1.grid(True)
            ax1.set_title("Discriminator probability on real patches: Validation set")
            ax1.set_xlabel('Epoch')
            ax1.set_ylabel('Probability')

            ax2.plot(epoch_list + 1, dis_guesses_fake_valid)
            ax2.grid(True)
            ax2.set_title("Discriminator probability on fake patches: Validation set")
            plt.xlabel('Epoch')
            plt.ylabel('Probability')
            # ax2.legend(["D(real)", "D(fake)"], loc="upper right")
            loss_figure_path = os.path.join("model_outputs/" + DCSRN_config.OUT_DIR_NAME,
                                            DCSRN_config.EXPERIMENT_NAME + "/loss_plots/probability_vs_epochs.png")

            fig.savefig(loss_figure_path)

            fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(10, 10))
            fig.subplots_adjust(hspace=0.5)

            ax1.plot(epoch_list + 1, psnr_valid_list)
            ax1.grid(True)
            ax1.set_title("Average Peak Signal to Noise Ratio (PSNR)")
            ax1.set_xlabel('Epoch')
            ax1.set_ylabel('PSNR')

            ax2.plot(epoch_list + 1, ssim_valid_list)
            ax2.grid(True)
            ax2.set_title("Average Structural Similarity Index (SSIM)")
            plt.xlabel('Epoch')
            plt.ylabel('SSIM')
            # ax2.legend(["D(real)", "D(fake)"], loc="upper right")
            loss_figure_path = os.path.join("model_outputs/" + DCSRN_config.OUT_DIR_NAME,
                                            DCSRN_config.EXPERIMENT_NAME + "/loss_plots/performance_metrics_vs_epochs.png")

            fig.savefig(loss_figure_path)


            plt.show()


    elif DCSRN_config.LOAD_PREVIOUS_MODELS == 0:
        # Load models if not already loaded
        # model = UNet().to(SRGAN_config.DEVICE)
        print("LOADING NETWORK PARAMETERS")
        load_path = os.path.join("saved_models/" + DCSRN_config.OUT_DIR_NAME, DCSRN_config.EXPERIMENT_NAME)
        generator.load_state_dict(torch.load(load_path + DCSRN_config.GENERATOR_NAME))
        discriminator.load_state_dict(torch.load(load_path + DCSRN_config.DISCRIMINATOR_NAME))


    # TEST POST-TRAINING GENERATOR ON TRAINING SET
    HR_train, LR_train = next(iter(test_loader))
    generator_output = generator(LR_train.to(DCSRN_config.DEVICE))

    if DCSRN_config.BATCH_SIZE == 1:
        I_HR = np.squeeze(HR_train.detach().cpu().numpy())
        I_SR = np.squeeze(generator_output.detach().cpu().numpy())

        plt.figure(figsize=(20, 10))

        resize = tiotransforms.Resize(target_shape=(
            DCSRN_config.PATCH_SIZE*DCSRN_config.UP_FACTOR,
            DCSRN_config.PATCH_SIZE*DCSRN_config.UP_FACTOR,
            DCSRN_config.PATCH_SIZE*DCSRN_config.UP_FACTOR),
            image_interpolation='NEAREST')
        I_interp = np.squeeze(resize(LR_train[0]))

        plt.subplot(1, 3, 1)
        plt.imshow(I_interp[:, :, I_interp.shape[-1] // 2], cmap='gray')
        plt.subplot(1, 3, 2)
        plt.imshow(I_SR[:, :, I_SR.shape[-1] // 2], cmap='gray')
        plt.subplot(1, 3, 3)
        plt.imshow(I_HR[:, :, I_HR.shape[-1] // 2], cmap='gray')
        plt.show()

    # Save model outputs as images for test data
    slices = [20, 30, 40, 50, 60]
    f_path = os.path.join("model_outputs/" + DCSRN_config.OUT_DIR_NAME, DCSRN_config.EXPERIMENT_NAME + "/train/")
    save_comparisons3D(train_loader, generator, f_path, slices,unnormalize=True)  # MAKE A 1x3 GRID OF TRAINING LR, SR, HR IMAGES

    f_path = os.path.join("model_outputs/" + DCSRN_config.OUT_DIR_NAME, DCSRN_config.EXPERIMENT_NAME + "/test/")
    save_comparisons3D(test_loader, generator, f_path, slices, unnormalize=True)  # MAKE A 1x3 GRID OF TRAINING LR, SR, HR IMAGES

    if DCSRN_config.use_dataset == "HCP_1200":
        # Save baseline model outputs for test data
        test_baseline = VOXELDATA(train=False,
                                 width=DCSRN_config.PATCH_SIZE_HR,
                                 height=DCSRN_config.PATCH_SIZE_HR,
                                 depth=DCSRN_config.PATCH_SIZE_HR,
                                 _transform_lr=DCSRN_config.TRANSFORM_LR,
                                 _transform_hr=DCSRN_config.TRANSFORM_HR,
                                 transforms_list=["gaussian_blur"],
                                 data_path=DCSRN_config.DATASET_PATH,
                                 train_path=DCSRN_config.TRAIN_PATH,
                                 test_path=DCSRN_config.TEST_PATH)
        test_loader_baseline = DataLoader(test_baseline, DCSRN_config.BATCH_SIZE, shuffle=False, num_workers=0, pin_memory=True)

        save_comparisons3D(test_loader_baseline, generator, f_path, slices, unnormalize=True, extra_suffix="_baseline")  # MAKE A 1x3 GRID OF TRAINING LR, SR, HR IMAGES
    else:
        if DCSRN_config.PRELOAD_BONE_DATA or DCSRN_config.PRELOAD_BONE_DATA_TEST:
            queue_length_test = 6  # 100
            samples_per_volume_test = 3
            base_sampler = tio.data.LabelSampler(DCSRN_config.PATCH_SIZE_HR, label_name="seg")
            base_subject = DCSRN_config.SUBJECTS_DATASET_TEST[0]
            base_subject['seg'].data = torch.zeros_like(base_subject['seg'].data)
            if DCSRN_config.PRELOAD_BONE_DATA_TEST:
                base_subject['seg'].data[0, 257, 290, 311] = 1.0  # 320x512x512
            else:
                #base_subject['seg'].data[0, 1290, 813, 701] = 1.0  # 701/1400, 1290/1700, 813/1600
                #base_subject['seg'].data[0, 611, 422, 701] = 1.0  # 701/1400, 611/1700, 422/1600
                base_subject['seg'].data[0, 701, 880, 1321] = 1.0
            base_subject_dataset = tio.SubjectsDataset([base_subject])
            test_dataset = CUSTOM_QUEUE(base_subject_dataset, queue_length_test, samples_per_volume_test, base_sampler, 1, False, False, None)

            test_loader_baseline = DataLoader(test_dataset, DCSRN_config.BATCH_SIZE, shuffle=False, num_workers=0, pin_memory=True)
            save_comparisons3D(test_loader_baseline, generator, f_path, slices, unnormalize=True, extra_suffix="_baseline")  # MAKE A 1x3 GRID OF TRAINING LR, SR, HR IMAGES

    if DCSRN_config.DO_FULL_RECONSTRUCTION:
        # Save full super-resolved MRI scan:
        from dataset_DCSRN import nvg_func
        data_path = os.path.join(DCSRN_config.DATASET_PATH, DCSRN_config.TEST_PATH)
        file_paths = glob.glob(os.path.join(data_path, nvg_func(DCSRN_config.use_dataset) + DCSRN_config.IMAGE_FORMAT))
        dim_x, dim_y, dim_z = DCSRN_config.DIM
        overlap = 4

        subject_lr = tio.Subject(one_image=tio.ScalarImage(file_paths[DCSRN_config.RECONSTRUCTION_IDX]))
        raw_image = subject_lr['one_image']['data']  #
        torch.save(raw_image, 'raw_image.pt')

        subject_hr = tio.Subject(one_image=tio.ScalarImage(tensor=torch.zeros(1, dim_x * 4, dim_y * 4, dim_z * 4)))
        grid_sampler_lr = tio.GridSampler(subject=subject_lr, patch_size=DCSRN_config.PATCH_SIZE, patch_overlap=overlap)
        grid_sampler_hr = tio.GridSampler(subject=subject_hr, patch_size=DCSRN_config.PATCH_SIZE_HR, patch_overlap=overlap*4)
        patch_loader_lr = torch.utils.data.DataLoader(grid_sampler_lr, batch_size=DCSRN_config.BATCH_SIZE)
        #aggregator_lr = tio.inference.GridAggregator(grid_sampler_lr)
        patch_loader_hr = torch.utils.data.DataLoader(grid_sampler_hr, batch_size=DCSRN_config.BATCH_SIZE)
        aggregator_hr = tio.inference.GridAggregator(grid_sampler_hr)

        n_patches = len(patch_loader_lr)
        c = 1
        with torch.no_grad():
            for patches_batch_lr, patches_batch_hr in zip(patch_loader_lr, patch_loader_hr):
                print("Reconstructing full 3D image: Patch batch %d / %d" % (c, n_patches))
                lr_patch = patches_batch_lr['one_image']['data']
                generator_output = generator(lr_patch.to(DCSRN_config.DEVICE))
                locations_hr = patches_batch_hr['location']
                aggregator_hr.add_batch(generator_output, locations_hr)
                c += 1

        full_sr_output = aggregator_hr.get_output_tensor()
        torch.save(full_sr_output, 'full_sr_output.pt')
        print("Full reconstruction size:", full_sr_output.size())

        if False:
            fig = plt.figure(figsize=(10, 10))
            plt.imshow(full_sr_output[0, :, :, dim_z * DCSRN_config.UP_FACTOR // 2], cmap='gray')
            plt.tick_params(left=False, right=False, labelleft=False, labelbottom=False, bottom=False)
            recon_figure_path = "model_outputs/" + DCSRN_config.OUT_DIR_NAME + "/full_reconstruction_1.png"
            fig.savefig(recon_figure_path)

            fig = plt.figure(figsize=(10, 10))
            plt.imshow(full_sr_output[0, dim_z * DCSRN_config.UP_FACTOR // 2, :, :], cmap='gray')
            plt.tick_params(left=False, right=False, labelleft=False, labelbottom=False, bottom=False)
            recon_figure_path = "model_outputs/" + DCSRN_config.OUT_DIR_NAME + "/full_reconstruction_2.png"
            fig.savefig(recon_figure_path)

        image_paths = "model_outputs/" + DCSRN_config.OUT_DIR_NAME + "/"
        image_names = ["full_reconstruction_1.png", "full_reconstruction_2.png", "full_reconstruction_3.png"]
        images = [full_sr_output[0, :, :, dim_z * DCSRN_config.UP_FACTOR // 2],
                  full_sr_output[0, :, dim_y * DCSRN_config.UP_FACTOR // 2, :],
                  full_sr_output[0, dim_x * DCSRN_config.UP_FACTOR // 2, :, :]]
        save_images_from_list(images, image_paths, image_names)

        # Save original image before super-resolution
        image_names = ["raw_image_1.png", "raw_image_2.png", "raw_image_3.png"]
        images = [raw_image[0, :, :, dim_z // 2],
                  raw_image[0, :, dim_y // 2, :],
                  raw_image[0, dim_x // 2, :, :]]
        save_images_from_list(images, image_paths, image_names)

        plt.show()

    elif False:
        print("LOADING FULL RECONSTRUCTION")
        full_sr_output = torch.load('full_sr_output.pt')

        image_paths = "model_outputs/" + DCSRN_config.OUT_DIR_NAME + "/"
        image_names = ["full_reconstruction_1.png", "full_reconstruction_2.png", "full_reconstruction_3.png"]
        images = [full_sr_output[0, :, :, dim_z * DCSRN_config.UP_FACTOR // 2],
                  full_sr_output[0, :, dim_y * DCSRN_config.UP_FACTOR // 2, :],
                  full_sr_output[0, dim_x * DCSRN_config.UP_FACTOR // 2, :, :]]
        save_images_from_list(images, image_paths, image_names)

        if False:
            recon_figure_path = "model_outputs/" + DCSRN_config.OUT_DIR_NAME + "/full_reconstruction_1.png"
            img_z = full_sr_output[0, :, :, dim_z * DCSRN_config.UP_FACTOR // 2]
            img_z = unnormalize_image(img_z).squeeze()
            img_z = rescale255(img_z)
            img_z = Image.fromarray(img_z)
            img_z.save(recon_figure_path)

            fig = plt.figure(figsize=(10,10))
            plt.imshow(img_z, cmap='gray')
            plt.tick_params(left=False, right=False, labelleft=False, labelbottom=False, bottom=False)
            fig.savefig(recon_figure_path)

            recon_figure_path = "model_outputs/" + DCSRN_config.OUT_DIR_NAME + "/full_reconstruction_2.png"
            img_x = full_sr_output[0, dim_z * DCSRN_config.UP_FACTOR // 2, :, :]
            img_x = unnormalize_image(img_x).squeeze()
            img_x = rescale255(img_x)
            img_x = Image.fromarray(img_x)
            img_x.save(recon_figure_path)

            fig = plt.figure(figsize=(10, 10))
            plt.imshow(img_x, cmap='gray')
            plt.tick_params(left=False, right=False, labelleft=False, labelbottom=False, bottom=False)
            fig.savefig(recon_figure_path)

        # Save original image before super-resolution
        raw_image = torch.load('raw_image.pt')
        image_names = ["raw_image_1.png", "raw_image_2.png", "raw_image_3.png"]
        images = [raw_image[0, :, :, dim_z // 2],
                  raw_image[0, :, dim_y // 2, :],
                  raw_image[0, dim_x // 2, :, :]]
        save_images_from_list(images, image_paths, image_names)

        #plt.show()

    #for patches_batch in patch_loader_lr:
    #    lr_patch = patches_batch['one_image']['data']
    #    generator_output = generator(lr_patch.to(DCSRN_config.DEVICE))
    #    locations = patches_batch['location']
    #    print(locations)

    #print(len(grid_sampler_lr))


    print("Done")

