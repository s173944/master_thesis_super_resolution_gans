
from tqdm import tqdm
import torch
import torch.nn as nn
import torchio as tio
import DCSRN_config
import kornia as korn
TOTAL_GPU_MEM = torch.cuda.get_device_properties(0).total_memory/10**9

#from DCSRN_GAN_test import compute_discriminator_loss, compute_generator_loss

def performance_metrics(real_hi_res, fake_hi_res):
    mse_func = nn.MSELoss()
    rescale = tio.transforms.RescaleIntensity((0.0, 1.0))
    psnr_total = 0
    ssim_total = 0
    L = 1.0  # Maximum value after scaling images between 0.0 and 1.0
    for real_patch, fake_patch in zip(real_hi_res, fake_hi_res):
        real_patch_rescaled = rescale(real_patch.cpu()).unsqueeze(0)
        fake_patch_rescaled = rescale(fake_patch.cpu()).unsqueeze(0)
        mse = mse_func(real_patch_rescaled, fake_patch_rescaled)
        psnr = 10*torch.log10((L**2)/mse)
        psnr_total += psnr

        ssim = torch.mean(korn.metrics.ssim3d(fake_patch_rescaled, real_patch_rescaled, window_size=11, max_val=L))
        ssim_total += ssim

    #avg_psnr = torch.mean(torch.Tensor(psnr_list))
    #avg_ssim = torch.mean(torch.Tensor(ssim_list))
    #max_real = torch.max(real_hi_res)
    #max_fake = torch.max(fake_hi_res)
    #max = torch.max(max_real, max_fake)

    #patch_ssim = torch.mean(korn.metrics.ssim3d(fake_hi_res, real_hi_res, window_size=11, max_val=max.item()))

    #patch_psnr = korn.metrics.psnr(fake_hi_res, real_hi_res, max_val=max_fake.item())

    return psnr_total, ssim_total


def compute_generator_loss_WGAN_GP(prop_fake, real_hi_res, fake_hi_res, loss_fn_dict, epoch):
    # Formulate Generator loss: Min -E[critic(gen_fake)]
    wasserstein_loss = 5e-3 * -torch.mean(prop_fake)  # from ESRGAN by Alladin Persson
    if epoch >= DCSRN_config.DISABLE_LOSSES_FROM_EPOCH:
        gen_loss = wasserstein_loss
    else:
        gen_loss_L1 = DCSRN_config.LOSS_WEIGHTS["L1"] * loss_fn_dict["L1_Loss"](real_hi_res, fake_hi_res)
        #gen_loss_MSE = DCSRN_config.LOSS_WEIGHTS["MSE"] * loss_fn_dict["MSE_Loss"](real_hi_res, fake_hi_res)
        gen_loss_GRAD = DCSRN_config.LOSS_WEIGHTS["GRAD"] * loss_fn_dict["GRAD_Loss"](real_hi_res, fake_hi_res)
        gen_loss_VGG3D = DCSRN_config.LOSS_WEIGHTS["VGG3D"] * loss_fn_dict["VGG3D_Loss"](real_hi_res, fake_hi_res)
        gen_loss_TV3D = DCSRN_config.LOSS_WEIGHTS["TV3D"] * loss_fn_dict["TV3D_Loss"](fake_hi_res)
        # gen_loss = gen_loss_MSE + gen_loss_TV3D + gen_loss_GRAD + gen_loss_VGG + adv_loss
        #gen_loss = gen_loss_L1 + gen_loss_TV3D + gen_loss_GRAD + gen_loss_VGG3D + wasserstein_loss
        gen_loss = gen_loss_L1 + gen_loss_VGG3D + gen_loss_TV3D + gen_loss_GRAD + wasserstein_loss
        #gen_loss = gen_loss_MSE + wasserstein_loss

    return gen_loss


def compute_discriminator_loss_WGAN_GP(critic, real_hi_res_critic, fake_hi_res_critic, prop_fake, prop_real):

    batch_size, C, H, W, D = real_hi_res_critic.shape
    epsilon = torch.rand((batch_size, 1, 1, 1, 1)).repeat(1, C, H, W, D).to(DCSRN_config.DEVICE)
    interpolated_batch = epsilon * real_hi_res_critic + (1 - epsilon) * fake_hi_res_critic
    interpolated_batch.requires_grad_(True)

    critic_scores = critic(interpolated_batch)

    gradient = torch.autograd.grad(
        inputs=interpolated_batch,
        outputs=critic_scores,
        grad_outputs=torch.ones_like(critic_scores),
        create_graph=True,
        retain_graph=True,
    )[0]

    gradient = gradient.view(gradient.shape[0], -1)
    gradient_norm = gradient.norm(2, dim=1)
    gradient_penalty = torch.mean((gradient_norm - 1) ** 2)

    #dis_loss = torch.mean(prop_fake) - torch.mean(prop_real) + DCSRN_config.LAMBDA_GP*gradient_penalty
    #print("Wasserstein distance:", -(torch.mean(prop_real) - torch.mean(prop_fake)))
    dis_loss = -(torch.mean(prop_real) - torch.mean(prop_fake)) + (DCSRN_config.LAMBDA_GP * gradient_penalty)
    return dis_loss, gradient_penalty

def gradient_penalty_func(critic, real_hi_res, fake_hi_res):

    batch_size, C, H, W, D = real_hi_res.shape
    epsilon = torch.rand((batch_size, 1, 1, 1, 1)).repeat(1, C, H, W, D).to(DCSRN_config.DEVICE)
    interpolated_batch = epsilon * real_hi_res + (1 - epsilon) * fake_hi_res
    interpolated_batch.requires_grad_(True)

    critic_scores = critic(interpolated_batch)

    gradient = torch.autograd.grad(
        inputs=interpolated_batch,
        outputs=critic_scores,
        grad_outputs=torch.ones_like(critic_scores),
        create_graph=True,
        retain_graph=True,
    )[0]

    gradient = gradient.view(gradient.shape[0], -1)
    gradient_norm = gradient.norm(2, dim=1)
    gradient_penalty = torch.mean((gradient_norm - 1)**2)
    return gradient_penalty

def train_mixed_precision_WGAN_GP_K1(generator, discriminator, opt_gen, opt_dis, loss_fn_dict, epochs, train_loader, test_loader, print_status):
    """
    Mixed precision training functions for mDCSRN-GAN
    :param generator:
    :param discriminator:
    :param opt_gen:
    :param opt_dis:
    :param loss_fn_dict:
    :param epochs:
    :param train_loader:
    :param test_loader:
    :param print_status:
    :return:
    """

    checkpoint = 0

    # Sigmoid function used for evaluating probability of discriminator
    sigmoid_func = nn.Sigmoid()

    dis_train_losses = []
    gen_train_losses = []
    dis_valid_losses = []  # This list is empty for WGAN-GP
    gen_valid_losses = []  # This list is empty for WGAN-GP
    psnr_valid_list = []
    ssim_valid_list = []
    dis_guesses_real_valid = []
    dis_guesses_fake_valid = []

    # Placeholders for validation losses/guesses
    dis_guess_real_valid, dis_guess_fake_valid = 0, 0
    # Placeholders for validation performance metrics
    psnr_valid, ssim_valid = 0, 0

    # Creates a GradScaler once at the beginning of training.
    gen_scaler = torch.cuda.amp.GradScaler()
    dis_scaler = torch.cuda.amp.GradScaler()

    for epoch in range(epochs):
        dis_train_loss, gen_train_loss = 0, 0

        torch.cuda.empty_cache()
        torch.cuda.reset_peak_memory_stats()
        discriminator.train()
        generator.train()

        #toggle_grad(generator, False)

        for batch_idx, (real_hi_res, lo_res) in enumerate(tqdm(train_loader, ascii=True, desc=("Epoch %d" % (epoch+1)), position=0, leave=False, ncols=80)):

            # Make sure to go through the entire dataset
            real_hi_res = real_hi_res.to(DCSRN_config.DEVICE)
            lo_res = lo_res.to(DCSRN_config.DEVICE)
            batch_size = real_hi_res.shape[0]

            for critic_ite in tqdm(range(DCSRN_config.CRITIC_ITERATIONS), ascii=True, desc=("Critic training"), position=1, leave=False, ncols=80):
                real_hi_res2, lo_res2 = next(iter(train_loader))
                real_hi_res2 = real_hi_res2.to(DCSRN_config.DEVICE)
                lo_res2 = lo_res2.to(DCSRN_config.DEVICE)

                with torch.cuda.amp.autocast(dtype=torch.float16):
                    # Generate fake high resolution images
                    with torch.no_grad():
                        fake_hi_res2 = generator(lo_res2)
                    # Compute discriminator probabilities on real and fake
                    prop_real = discriminator(real_hi_res2)
                    prop_fake = discriminator(fake_hi_res2.detach())

                    # Compute discriminator loss based on WGAN
                    dis_loss, gp = compute_discriminator_loss_WGAN_GP(discriminator, real_hi_res2, fake_hi_res2, prop_fake, prop_real)

                    # Compute average epoch loss
                    dis_train_loss += dis_loss.item() / len(train_loader)
                    #print(f"Critic loss {dis_train_loss:.4f}")

                # Train discriminator
                opt_dis.zero_grad()  # set parameter gradients to zero
                dis_scaler.scale(dis_loss).backward()  # backward-pass to compute gradients
                dis_scaler.step(opt_dis)  # update weights
                dis_scaler.update()
                #print(f"Critic training iteration: {critic_ite+1}")

            #generator.train()  # Set the model to train mode. This will enable dropout and so on if implemented
            #discriminator.eval()  # Disable discriminator after training CRITIC_ITERATIONS times
            with torch.cuda.amp.autocast(dtype=torch.float16):
                fake_hi_res = generator(lo_res)
                #with torch.no_grad():
                prop_fake = discriminator(fake_hi_res)  # Compute prop_fake again, as the value was discarded by backward
                #prop_fake = discriminator(fake_hi_res)  # Compute prop_fake again, as the value was discarded by backward
                gen_loss = compute_generator_loss_WGAN_GP(prop_fake, real_hi_res, fake_hi_res, loss_fn_dict, epoch)
                #gen_loss = DCSRN_config.LOSS_WEIGHTS["MSE"] * loss_fn_dict["MSE_Loss"](real_hi_res, fake_hi_res)
                #gen_loss = 5e-3 * -torch.mean(discriminator(fake_hi_res)) + DCSRN_config.LOSS_WEIGHTS["MSE"] * loss_fn_dict["MSE_Loss"](real_hi_res, fake_hi_res)

            # Compute average epoch loss
            gen_train_loss += gen_loss.item() / len(train_loader)
            #print(f"Generator loss {gen_train_loss:.4f}")

            # Train generator
            opt_gen.zero_grad()
            gen_scaler.scale(gen_loss).backward()
            #print("Memory allocation gen backward: %0.3f Gb / %0.3f Gb" % (torch.cuda.memory_allocated() / 10 ** 9, TOTAL_GPU_MEM))
            gen_scaler.step(opt_gen)
            gen_scaler.update()

        print(f"Discriminator training loss in epoch {epoch + 1}/{epochs} was {dis_train_loss:.4f}")
        print(f"Generator GAN training loss in epoch {epoch + 1}/{epochs} was {gen_train_loss:.4f}")
        dis_train_losses.append(dis_train_loss)
        gen_train_losses.append(gen_train_loss)

        # Validation step every N epoch
        # Evaluate, do not propagate gradients
        #if DCSRN_config.VALIDATION_FREQUENCY != -1:
        if (epoch % DCSRN_config.VALIDATION_FREQUENCY == 0):
            # Placeholders for losses
            dis_guess_real_valid, dis_guess_fake_valid = 0, 0
            # Placeholders for performance metrics
            psnr_valid, ssim_valid = 0, 0

            with torch.inference_mode():
                generator.eval()
                discriminator.eval()

                # Just load a single batch from the test loader
                #real_hi_res, lo_res = next(iter(test_loader))
                # Or validate using the whole validation set
                for batch_idx, (real_hi_res, lo_res) in enumerate(test_loader):
                    real_hi_res = real_hi_res.to(DCSRN_config.DEVICE)
                    lo_res = lo_res.to(DCSRN_config.DEVICE)

                    with torch.cuda.amp.autocast(dtype=torch.float16):
                        # Generate fake high resolution images
                        fake_hi_res = generator(lo_res)
                        prop_real = discriminator(real_hi_res)
                        prop_fake = discriminator(fake_hi_res.detach())

                        #dis_loss = compute_discriminator_loss_WGAN_GP(discriminator, real_hi_res, fake_hi_res, prop_fake, prop_real)

                    # Compute average epoch loss
                    #dis_valid_loss += dis_loss.item() / len(test_loader)

                    # Compute average discriminator probability on fake and real
                    dis_guess_real_valid += (torch.sum(sigmoid_func(prop_real))).item() / test_loader.sampler.num_samples
                    dis_guess_fake_valid += (torch.sum(sigmoid_func(prop_fake))).item() / test_loader.sampler.num_samples

                    psnr, ssim = performance_metrics(real_hi_res, fake_hi_res)
                    psnr_valid += psnr.item() / test_loader.sampler.num_samples
                    ssim_valid += ssim.item() / test_loader.sampler.num_samples

                    #with torch.cuda.amp.autocast(dtype=torch.float16):
                    #    prop_fake = discriminator(fake_hi_res)  # Compute prop_fake again, as the value was discarded by backward
                        #gen_loss = compute_generator_loss(prop_fake, real_hi_res, fake_hi_res, loss_fn_dict, epoch)
                    #    gen_loss = compute_generator_loss_WGAN_GP(prop_fake, real_hi_res, fake_hi_res, loss_fn_dict, epoch)

                    # Compute average epoch loss
                    #gen_valid_loss += gen_loss.item() / len(test_loader)

            #dis_guess_real_valid = dis_guess_real_valid.cpu().detach()
            #dis_guess_fake_valid = dis_guess_fake_valid.cpu().detach()
            #psnr_valid = psnr_valid.cpu().detach()
            #ssim_valid = ssim_valid.cpu().detach()

        # Append psnr, ssim and guesses for real and fake.
        # Values are appended every epoch such that these lists have the same length as number of epochs
        # The values only change when the validation step is run
        print(f"Average PSNR of validation set in epoch {epoch + 1}/{epochs} was {psnr_valid:.4f}")
        print(f"Average SSIM of validation set in epoch {epoch + 1}/{epochs} was {ssim_valid:.4f}")
        print(f"Average critic guess on reals in epoch {epoch + 1}/{epochs} was {dis_guess_real_valid:.4f}")
        print(f"Average critic guess on fakes in epoch {epoch + 1}/{epochs} was {dis_guess_fake_valid:.4f}")

        psnr_valid_list.append(psnr_valid)
        ssim_valid_list.append(ssim_valid)
        dis_guesses_real_valid.append(dis_guess_real_valid)
        dis_guesses_fake_valid.append(dis_guess_fake_valid)

        max_memory_allocated = torch.cuda.max_memory_allocated()
        max_memory_reserved = torch.cuda.max_memory_reserved()
        print("Maximum memory reserved during epoch: %0.3f Gb / %0.3f Gb" % (
        max_memory_reserved / 10 ** 9, TOTAL_GPU_MEM))


        if (epoch > 0) and ((epoch+1) % DCSRN_config.CHECKPOINT_FREQUENCY == 0):  # Save network parameters every 50 epoch
            checkpoint_str = ("CP_%d_" % (epoch+1))
            # SAVE MODEL
            print(("SAVING NETWORK PARAMETER CHECKPOINT AT EPOCH %d" % (epoch+1)))
            torch.save(generator.state_dict(), "saved_models/" + checkpoint_str + DCSRN_config.GENERATOR_NAME)
            torch.save(discriminator.state_dict(), "saved_models/" + checkpoint_str + DCSRN_config.DISCRIMINATOR_NAME)

    out_dict = {
        "Discriminator_train_losses": dis_train_losses,
        "Generator_train_losses": gen_train_losses,
        "Discriminator_valid_losses": dis_valid_losses,
        "Generator_valid_losses": gen_valid_losses,
        "PSNR_valid_scores": psnr_valid_list,
        "SSIM_valid_scores": ssim_valid_list,
        "Discriminator_guesses_fake_valid": dis_guesses_fake_valid,
        "Discriminator_guesses_real_valid": dis_guesses_real_valid

    }

    return out_dict


def train_mixed_precision_WGAN_GP_WORKS(generator, discriminator, opt_gen, opt_dis, loss_fn_dict, epochs, train_loader, test_loader, print_status):
    """
    Mixed precision training functions for mDCSRN-GAN
    :param generator:
    :param discriminator:
    :param opt_gen:
    :param opt_dis:
    :param loss_fn_dict:
    :param epochs:
    :param train_loader:
    :param test_loader:
    :param print_status:
    :return:
    """

    # Sigmoid function used for evaluating probability of discriminator
    sigmoid_func = nn.Sigmoid()

    dis_train_losses = []
    gen_train_losses = []
    dis_valid_losses = []
    gen_valid_losses = []
    dis_guesses_real_valid = []
    dis_guesses_fake_valid = []

    # Creates a GradScaler once at the beginning of training.
    scaler = torch.cuda.amp.GradScaler()

    for epoch in range(epochs):
        dis_train_loss, gen_train_loss = 0, 0

        torch.cuda.empty_cache()
        torch.cuda.reset_peak_memory_stats()
        discriminator.train()
        generator.eval()

        #toggle_grad(generator, False)

        for batch_idx, (real_hi_res, lo_res) in enumerate(train_loader):

            for p in discriminator.parameters():
                p.requires_grad = True  # Enable gradients of discriminator/critic

            for critic_ite in range(DCSRN_config.CRITIC_ITERATIONS):

                # Sample batch for discriminator
                real_hi_res_critic, lo_res_critic = next(iter(train_loader))
                # Load batches onto GPU
                real_hi_res_critic = real_hi_res_critic.to(DCSRN_config.DEVICE)
                lo_res_critic = lo_res_critic.to(DCSRN_config.DEVICE)

                with torch.cuda.amp.autocast(dtype=torch.float16):
                    # Generate fake high resolution images
                    fake_hi_res_critic = generator(lo_res_critic)
                    # Compute discriminator probabilities on real and fake
                    prop_real = discriminator(real_hi_res_critic)
                    prop_fake = discriminator(fake_hi_res_critic.detach())

                    # Compute discriminator loss based on WGAN
                    dis_loss = compute_discriminator_loss_WGAN_GP(discriminator, real_hi_res_critic, fake_hi_res_critic, prop_fake, prop_real)

                # Compute average epoch loss
                dis_train_loss += dis_loss.item() / len(train_loader)

                # Train discriminator
                opt_dis.zero_grad()  # set parameter gradients to zero
                scaler.scale(dis_loss).backward()  # backward-pass to compute gradients
                scaler.step(opt_dis)  # update weights
                scaler.update()
                print(f"Critic training iteration: {critic_ite+1}")

            # Make sure to go through the entire dataset
            real_hi_res = real_hi_res.to(DCSRN_config.DEVICE)
            lo_res = lo_res.to(DCSRN_config.DEVICE)
            #batch_size = real_hi_res.shape[0]

            # Now train generator once
            print("Training generator")
            for p in discriminator.parameters():
                p.requires_grad = False  # to avoid computation

            generator.train()  # Set the model to train mode. This will enable dropout and so on if implemented
            discriminator.eval()  # Disable discriminator after training CRITIC_ITERATIONS times
            with torch.cuda.amp.autocast(dtype=torch.float16):
                fake_hi_res = generator(lo_res)
                with torch.no_grad():
                    prop_fake = discriminator(fake_hi_res)  # Compute prop_fake again, as the value was discarded by backward
                gen_loss = compute_generator_loss_WGAN_GP(prop_fake, real_hi_res, fake_hi_res, loss_fn_dict, epoch)

            # Compute average epoch loss
            gen_train_loss += gen_loss.item() / len(train_loader)

            # Train generator
            opt_gen.zero_grad()
            scaler.scale(gen_loss).backward()
            #print("Memory allocation gen backward: %0.3f Gb / %0.3f Gb" % (torch.cuda.memory_allocated() / 10 ** 9, TOTAL_GPU_MEM))
            scaler.step(opt_gen)
            scaler.update()

        max_memory_allocated = torch.cuda.max_memory_allocated()
        max_memory_reserved = torch.cuda.max_memory_reserved()
        print("Maximum memory reserved during training step: %0.3f Gb / %0.3f Gb" % (max_memory_reserved / 10 ** 9, TOTAL_GPU_MEM))

        if DCSRN_config.VALIDATION_FREQUENCY == -1:
            if epoch >= DCSRN_config.DISABLE_LOSSES_FROM_EPOCH:
                print("LOSSES DISABLED EXCEPT ADVERSARIAL LOSS")
            print(f"Discriminator training loss in epoch {epoch + 1}/{epochs} was {dis_train_loss:.4f}")
            print(f"Generator GAN training loss in epoch {epoch + 1}/{epochs} was {gen_train_loss:.4f}")
            dis_train_losses.append(dis_train_loss)
            gen_train_losses.append(gen_train_loss)

        else:
            # Validation step every N epoch
            # Evaluate, do not propagate gradients
            dis_valid_loss, gen_valid_loss, dis_guess_real_valid, dis_guess_fake_valid = 0, 0, 0, 0
            if epoch % DCSRN_config.VALIDATION_FREQUENCY == 0:
                #with torch.no_grad():
                generator.eval()
                discriminator.eval()

                # Just load a single batch from the test loader
                #real_hi_res, lo_res = next(iter(test_loader))
                # Or validate using the whole validation set
                for batch_idx, (real_hi_res, lo_res) in enumerate(test_loader):
                    real_hi_res = real_hi_res.to(DCSRN_config.DEVICE)
                    lo_res = lo_res.to(DCSRN_config.DEVICE)

                    with torch.cuda.amp.autocast(dtype=torch.float16):
                        # Generate fake high resolution images
                        fake_hi_res = generator(lo_res)
                        prop_real = discriminator(real_hi_res)
                        prop_fake = discriminator(fake_hi_res.detach())
                        #dis_loss = compute_discriminator_loss(prop_real, prop_fake, loss_fn_dict)
                        dis_loss = compute_discriminator_loss_WGAN_GP(discriminator, real_hi_res, fake_hi_res, prop_fake, prop_real)

                    # Compute average epoch loss
                    dis_valid_loss += dis_loss.item() / len(test_loader)

                    # Compute average discriminator probability on fake and real
                    dis_guess_real_valid += torch.sum(sigmoid_func(prop_real)) / test_loader.sampler.num_samples
                    dis_guess_fake_valid += torch.sum(sigmoid_func(prop_fake)) / test_loader.sampler.num_samples

                    with torch.cuda.amp.autocast(dtype=torch.float16):
                        prop_fake = discriminator(fake_hi_res)  # Compute prop_fake again, as the value was discarded by backward
                        #gen_loss = compute_generator_loss(prop_fake, real_hi_res, fake_hi_res, loss_fn_dict, epoch)
                        gen_loss = compute_generator_loss_WGAN_GP(prop_fake, real_hi_res, fake_hi_res, loss_fn_dict, epoch)

                    # Compute average epoch loss
                    gen_valid_loss += gen_loss.item() / len(test_loader)

            if print_status:
                if epoch >= DCSRN_config.DISABLE_LOSSES_FROM_EPOCH:
                    print("LOSSES DISABLED EXCEPT ADVERSARIAL LOSS")
                print(f"Discriminator training/validation loss in epoch {epoch+1}/{epochs} was {dis_train_loss:.4f}/{dis_valid_loss:.4f}")
                print(f"Generator GAN training/validation loss in epoch {epoch+1}/{epochs} was {gen_train_loss:.4f}/{gen_valid_loss:.4f}")
                dis_train_losses.append(dis_train_loss)
                gen_train_losses.append(gen_train_loss)
                dis_valid_losses.append(dis_valid_loss)
                gen_valid_losses.append(gen_valid_loss)
                dis_guesses_real_valid.append(dis_guess_real_valid.cpu().detach())
                dis_guesses_fake_valid.append(dis_guess_fake_valid.cpu().detach())

    if DCSRN_config.VALIDATION_FREQUENCY == -1:
        return [dis_train_losses, gen_train_losses]
    else:
        return [dis_train_losses, gen_train_losses, dis_valid_losses, gen_valid_losses, dis_guesses_real_valid, dis_guesses_fake_valid]

def train_mixed_precision_WGAN_GP_V2(generator, discriminator, opt_gen, opt_dis, loss_fn_dict, epochs, train_loader, test_loader, print_status):
    """
    Mixed precision training functions for mDCSRN-GAN
    :param generator:
    :param discriminator:
    :param opt_gen:
    :param opt_dis:
    :param loss_fn_dict:
    :param epochs:
    :param train_loader:
    :param test_loader:
    :param print_status:
    :return:
    """

    # Sigmoid function used for evaluating probability of discriminator
    sigmoid_func = nn.Sigmoid()

    dis_train_losses = []
    gen_train_losses = []
    dis_valid_losses = []
    gen_valid_losses = []
    dis_guesses_real_valid = []
    dis_guesses_fake_valid = []

    # Creates a GradScaler once at the beginning of training.
    dis_scaler = torch.cuda.amp.GradScaler()
    gen_scaler = torch.cuda.amp.GradScaler()

    for epoch in range(epochs):
        dis_train_loss, gen_train_loss = 0, 0

        torch.cuda.empty_cache()
        torch.cuda.reset_peak_memory_stats()

        #toggle_grad(generator, False)

        for batch_idx, (real_hi_res, lo_res) in enumerate(train_loader):

            for p in discriminator.parameters():
                p.requires_grad = True  # Enable gradients of discriminator/critic

            discriminator.train()
            generator.eval()

            # Load batches onto GPU
            real_hi_res = real_hi_res.to(DCSRN_config.DEVICE)
            lo_res = lo_res.to(DCSRN_config.DEVICE)

            with torch.cuda.amp.autocast(dtype=torch.float16):
                # Generate fake high resolution images
                fake_hi_res = generator(lo_res)
                # Compute discriminator probabilities on real and fake
                prop_real = discriminator(real_hi_res)
                prop_fake = discriminator(fake_hi_res.detach())

                # Compute discriminator loss based on WGAN
                dis_loss = compute_discriminator_loss_WGAN_GP(discriminator, real_hi_res, fake_hi_res, prop_fake, prop_real)

            # Compute average epoch loss
            dis_train_loss += dis_loss.item() / len(train_loader)

            # Train discriminator
            opt_dis.zero_grad()  # set parameter gradients to zero
            dis_scaler.scale(dis_loss).backward(retain_graph=True)  # backward-pass to compute gradients
            #dis_scaler.scale(dis_loss).backward()  # backward-pass to compute gradients
            dis_scaler.step(opt_dis)  # update weights
            dis_scaler.update()

            # Now train generator
            for p in discriminator.parameters():
                p.requires_grad = False  # to avoid computation

            discriminator.eval()  # Disable discriminator after training CRITIC_ITERATIONS times
            generator.train()  # Set the model to train mode. This will enable dropout and so on if implemented

            with torch.cuda.amp.autocast(dtype=torch.float16):
                # No-grad for discriminator here ?
                prop_fake = discriminator(fake_hi_res)  # Compute prop_fake again, as the value was discarded by backward
                gen_loss = compute_generator_loss_WGAN_GP(prop_fake, real_hi_res, fake_hi_res, loss_fn_dict, epoch)

            # Compute average epoch loss
            gen_train_loss += gen_loss.item() / len(train_loader)

            # Train generator
            opt_gen.zero_grad()
            gen_scaler.scale(gen_loss).backward()
            #print("Memory allocation gen backward: %0.3f Gb / %0.3f Gb" % (torch.cuda.memory_allocated() / 10 ** 9, TOTAL_GPU_MEM))
            gen_scaler.step(opt_gen)
            gen_scaler.update()

        max_memory_allocated = torch.cuda.max_memory_allocated()
        max_memory_reserved = torch.cuda.max_memory_reserved()
        print("Maximum memory reserved during training step: %0.3f Gb / %0.3f Gb" % (max_memory_reserved / 10 ** 9, TOTAL_GPU_MEM))

        if print_status:
            if DCSRN_config.VALIDATION_FREQUENCY == -1:
                if epoch >= DCSRN_config.DISABLE_LOSSES_FROM_EPOCH:
                    print("LOSSES DISABLED EXCEPT ADVERSARIAL LOSS")
                print(f"Discriminator training loss in epoch {epoch + 1}/{epochs} was {dis_train_loss:.4f}")
                print(f"Generator GAN training loss in epoch {epoch + 1}/{epochs} was {gen_train_loss:.4f}")
                dis_train_losses.append(dis_train_loss)
                gen_train_losses.append(gen_train_loss)

    return [dis_train_losses, gen_train_losses]




if __name__ == "__main__":

    print("Done")